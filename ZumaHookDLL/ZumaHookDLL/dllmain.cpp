// dllmain.cpp : Defines the entry point for the DLL application.
#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <winternl.h>
#include <string>
#include <sstream>

#include "dlldefs.h"
#include "functions.h"

#include "Shlwapi.h"
#pragma comment (lib, "Shlwapi.lib")

#define BREAK __asm int 3


HMODULE exeModule;
HMODULE dllModule;

std::wstring currentdllPath;
std::wstring currentdllName;
std::wstring procPath;
std::wstring procName;

void DllHide();

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	size_t i;

	if (ul_reason_for_call == DLL_PROCESS_ATTACH) 
	{
		dllModule = hModule;

		currentdllPath.resize(MAX_PATH);
		i = GetModuleFileNameW(hModule, &currentdllPath[0], MAX_PATH);
		currentdllPath = currentdllPath.substr(0, i);

		//DllHide(); 

		exeModule = GetModuleHandle(NULL);

		procPath.resize(MAX_PATH);
		i = GetModuleFileNameW(NULL, &procPath[0], MAX_PATH);
		procPath = procPath.substr(0, i);

		procName = std::wstring(PathFindFileNameW(procPath.c_str()));
		currentdllName = std::wstring(PathFindFileNameW(currentdllPath.c_str()));

	}

	BYTE* increaseScoreAddr = (BYTE*)((DWORD)exeModule + INCREASE_SCORE_FUNC_OFFSET);
	BYTE* increaseScoreHookAddr = (BYTE*)increaseScoreHook; //(BYTE*)GetProcAddress(hModule, "increaseScoreHook");
	DWORD relative = (DWORD)increaseScoreHookAddr - (DWORD)increaseScoreAddr;

	std::wostringstream oss;
	oss << std::hex << " r:" << relative << " i:" << (DWORD)increaseScoreHookAddr << " mod:" << (DWORD)hModule;

	std::wstring msg;
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		msg = currentdllName + std::wstring(L" was loaded into ") + procName + L" " + oss.str();
		
		increaseScoreAddr = (BYTE*)((DWORD)exeModule + INCREASE_SCORE_FUNC_OFFSET);

		memcpy(increaseScoreAddr - 8, increaseScoreAddr, 3);

		*(increaseScoreAddr + 0) = 0xEB; // jmp (of 8bits)
		*(increaseScoreAddr + 1) = 0xF6; // EIP-10
		*(increaseScoreAddr + 2) = 0x90; // NOP

		relative = increaseScoreHookAddr - increaseScoreAddr;

		*(increaseScoreAddr - 5) = 0xE9; // jmp (of 32bits)

		*(DWORD*)(increaseScoreAddr - 4) = relative;
		//*(DWORD*)(increaseScoreAddr - 4) = 0x3; // no jump back to original function
		
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

void delFromModuleList(DWORD moduleListOffset, const wchar_t* dllName)
// '0x00' - load order
// '0x08' - mem order
// '0x10' - init order
{
	PEB* pebBase = NULL;

	__asm	push eax
	__asm	mov eax, FS:[0x30]
		__asm	mov[pebBase], eax
	__asm	pop eax

	PEB_LDR_DATA_R* pLdrData = (PEB_LDR_DATA_R*)pebBase->Ldr;

	PLIST_ENTRY moduleListHead = NULL;
	PLIST_ENTRY moduleListPtr = NULL;

	switch (moduleListOffset)
	{
	case 0x00: // load order
		moduleListPtr = &(pLdrData->InLoadOrderModuleList);
		break;
	case 0x08: // mem order
		moduleListPtr = &(pLdrData->InMemoryOrderModuleList);
		break;
	case 0x10: // init order
		moduleListPtr = &(pLdrData->InInitializationOrderModuleList);
		break;
	default:
		break;
	}

	moduleListHead = moduleListPtr;

	UNICODE_STRING* moduleName = NULL;
	LDR_MODULE* currentModule = NULL;
	do
	{
		moduleListPtr = moduleListPtr->Flink;
		currentModule = (LDR_MODULE*)((DWORD)moduleListPtr - moduleListOffset);

		moduleName = &(currentModule->FullDllName);

		if (0 == wcsncmp(moduleName->Buffer, dllName, moduleName->Length / 2))
		{
			(moduleListPtr->Blink)->Flink = moduleListPtr->Flink;
			(moduleListPtr->Flink)->Blink = moduleListPtr->Blink;

			moduleListPtr = moduleListPtr->Blink;

			/*
			if (moduleListOffset == 0x10)
			{
			ZeroMemory(currentModule, sizeof(LDR_MODULE));
			}*/
			// overwrite memory here to prevent recovery, but only when this function is called the last time
			// not working -> crashes zuma at exit
		}


	} while (moduleListPtr->Flink != moduleListHead);

}

void DllHide()
{
	std::wstring currentdllPath;
	currentdllPath.resize(MAX_PATH);
	int i = GetModuleFileNameW(dllModule, &currentdllPath[0], MAX_PATH);
	currentdllPath = currentdllPath.substr(0, i);

	delFromModuleList(0x00, currentdllPath.c_str());
	delFromModuleList(0x08, currentdllPath.c_str());
	delFromModuleList(0x10, currentdllPath.c_str());
}
