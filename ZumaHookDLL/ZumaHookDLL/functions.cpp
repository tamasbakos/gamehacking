#include "functions.h"

#define SCORE_PTR_ADDR 0x0018FDD4


//void __fastcall increaseScoreHook(void *pthis, DWORD edx, DWORD toAdd, DWORD one)
__declspec(naked)
void increaseScoreHook()
{
	__asm sub esp, 8
	__asm pushad

	DWORD *pCurrentScore;
	DWORD *pScoreToAdd;

	__asm mov eax, ecx
	__asm add eax, 0xB8
	__asm mov[pCurrentScore], eax

	__asm mov eax, ebp
	__asm add eax, 8
	__asm mov[pScoreToAdd], eax

	__asm mov eax, [eax]
	__asm mov eax, [ecx + 0xB8]

	*pScoreToAdd *= 10;

	__asm popad
	__asm add esp, 8

	__asm mov eax, [exeModule]
	__asm add eax, INCREASE_SCORE_FUNC_OFFSET + 3

	__asm jmp eax
}