#pragma once

#include <Windows.h>
#include <winternl.h>

#define INCREASE_SCORE_FUNC_OFFSET 0x13A40

extern HMODULE exeModule;
extern HMODULE dllModule;

extern std::wstring currentdllPath;
extern std::wstring currentdllName;
extern std::wstring procPath;
extern std::wstring procName;

typedef struct _PEB_LDR_DATA_R
{
	ULONG                   Length;
	BOOLEAN                 Initialized;
	PVOID                   SsHandle;
	LIST_ENTRY              InLoadOrderModuleList;
	LIST_ENTRY              InMemoryOrderModuleList;
	LIST_ENTRY              InInitializationOrderModuleList;
} PEB_LDR_DATA_R, *PPEB_LDR_DATA_R;

typedef struct _LDR_MODULE {

	LIST_ENTRY              InLoadOrderModuleList;
	LIST_ENTRY              InMemoryOrderModuleList;
	LIST_ENTRY              InInitializationOrderModuleList;
	PVOID                   BaseAddress;
	PVOID                   EntryPoint;
	ULONG                   SizeOfImage;
	UNICODE_STRING          FullDllName;
	UNICODE_STRING          BaseDllName;
	ULONG                   Flags;
	SHORT                   LoadCount;
	SHORT                   TlsIndex;
	LIST_ENTRY              HashTableEntry;
	ULONG                   TimeDateStamp;

} LDR_MODULE, *PLDR_MODULE;