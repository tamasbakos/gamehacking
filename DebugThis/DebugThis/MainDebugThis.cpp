#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include <Psapi.h>
#include <TlHelp32.h>
#include <iostream>
#include <vector>
#include <cstdio>

#include <winternl.h>
#include <wdmguid.h>

#include "internals.h"


#pragma comment(lib,"ntdll.lib")

void func1()
{
	while (true)
	{
		std::cout << "Thread " << std::hex << GetCurrentThreadId() << std::endl;
		Sleep(1000);
		//break;
	}
}

void func2()
{
	while (true)
	{
		std::cout << "Thread two  -" << std::endl;
		Sleep(800);
	}
}

void dump(HANDLE file, std::string text, void* ptr, int from, int to)
{
	char buf[4];

	WriteFile(file, text.c_str(), text.size(), NULL, NULL);

	for (int i = from; i < to; ++i)
	{
		BYTE wr = *((BYTE*)ptr + i);
		sprintf(buf, "%02X ", wr);
		WriteFile(file, &buf, 3, NULL, NULL);
	}
	text = "\r\n\r\n";
	WriteFile(file, text.c_str(), text.size(), NULL, NULL);
}

int main()
{

	HANDLE thread1, thread2;

	thread1 = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)func1, NULL, NULL, 0);
	//thread2 = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)func2, NULL, NULL, 0);

	//HMODULE mod = LoadLibraryW(L"ll.dll");
	//HMODULE mod = LoadLibraryW(L"CustomDLL64.dll");
	HANDLE current = GetCurrentProcess();

	HMODULE localDllHandle = GetModuleHandleW(L"Kernel32");
	FARPROC loadLibAddr = GetProcAddress(localDllHandle, "LoadLibraryW");

	BYTE* loadLibraryRelativeAddr = (BYTE*)((long long)loadLibAddr - (long long)localDllHandle);
	int i = 0;

	boolean loop = true;

	int e = 200;

	while (loop)
	{
		std::cout << "Thread Main " << std::hex << GetCurrentThreadId() << std::endl;

		std::vector<HMODULE> remoteMods(1024);
		DWORD remoteModsSize;
		EnumProcessModulesEx(current, &remoteMods[0], remoteMods.size() * sizeof(HMODULE), &remoteModsSize, LIST_MODULES_ALL);

		wchar_t modName[MAX_PATH];
		for (int i = 0; i < remoteModsSize / sizeof(HMODULE); ++i)
		{
			GetModuleFileNameW(remoteMods[i], modName, MAX_PATH);

			//std::wcout << modName << std::endl;
		}

		Sleep(2000);
	}

	DWORD wait_here = 0;
	//__asm int 3

	return 0;
}