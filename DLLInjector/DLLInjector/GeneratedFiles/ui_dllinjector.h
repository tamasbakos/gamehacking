/********************************************************************************
** Form generated from reading UI file 'dllinjector.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLLINJECTOR_H
#define UI_DLLINJECTOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DLLInjectorClass
{
public:
    QWidget *centralWidget;
    QComboBox *comboBox_procList;
    QLabel *label_procList;
    QPushButton *pushButton_reloadProcList;
    QListView *listView_DLLs;
    QPushButton *pushButton_removeDLL;
    QPushButton *pushButton_clearDDLs;
    QPushButton *pushButton_addDLL;
    QPushButton *pushButton_injectDLL;
    QPushButton *pushButton_selectProc;
    QLineEdit *lineEdit_searchProc;
    QPushButton *pushButton_searchProc;
    QPushButton *pushButton_reselectProc;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *DLLInjectorClass)
    {
        if (DLLInjectorClass->objectName().isEmpty())
            DLLInjectorClass->setObjectName(QStringLiteral("DLLInjectorClass"));
        DLLInjectorClass->resize(510, 340);
        centralWidget = new QWidget(DLLInjectorClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        comboBox_procList = new QComboBox(centralWidget);
        comboBox_procList->setObjectName(QStringLiteral("comboBox_procList"));
        comboBox_procList->setGeometry(QRect(20, 30, 231, 22));
        label_procList = new QLabel(centralWidget);
        label_procList->setObjectName(QStringLiteral("label_procList"));
        label_procList->setGeometry(QRect(20, 10, 111, 21));
        pushButton_reloadProcList = new QPushButton(centralWidget);
        pushButton_reloadProcList->setObjectName(QStringLiteral("pushButton_reloadProcList"));
        pushButton_reloadProcList->setGeometry(QRect(260, 30, 111, 21));
        listView_DLLs = new QListView(centralWidget);
        listView_DLLs->setObjectName(QStringLiteral("listView_DLLs"));
        listView_DLLs->setGeometry(QRect(20, 90, 351, 192));
        pushButton_removeDLL = new QPushButton(centralWidget);
        pushButton_removeDLL->setObjectName(QStringLiteral("pushButton_removeDLL"));
        pushButton_removeDLL->setGeometry(QRect(380, 180, 111, 23));
        pushButton_clearDDLs = new QPushButton(centralWidget);
        pushButton_clearDDLs->setObjectName(QStringLiteral("pushButton_clearDDLs"));
        pushButton_clearDDLs->setGeometry(QRect(380, 220, 111, 23));
        pushButton_addDLL = new QPushButton(centralWidget);
        pushButton_addDLL->setObjectName(QStringLiteral("pushButton_addDLL"));
        pushButton_addDLL->setGeometry(QRect(380, 140, 111, 23));
        pushButton_injectDLL = new QPushButton(centralWidget);
        pushButton_injectDLL->setObjectName(QStringLiteral("pushButton_injectDLL"));
        pushButton_injectDLL->setGeometry(QRect(380, 90, 111, 31));
        pushButton_selectProc = new QPushButton(centralWidget);
        pushButton_selectProc->setObjectName(QStringLiteral("pushButton_selectProc"));
        pushButton_selectProc->setGeometry(QRect(380, 30, 111, 21));
        lineEdit_searchProc = new QLineEdit(centralWidget);
        lineEdit_searchProc->setObjectName(QStringLiteral("lineEdit_searchProc"));
        lineEdit_searchProc->setGeometry(QRect(20, 60, 231, 20));
        pushButton_searchProc = new QPushButton(centralWidget);
        pushButton_searchProc->setObjectName(QStringLiteral("pushButton_searchProc"));
        pushButton_searchProc->setGeometry(QRect(260, 60, 111, 21));
        pushButton_reselectProc = new QPushButton(centralWidget);
        pushButton_reselectProc->setObjectName(QStringLiteral("pushButton_reselectProc"));
        pushButton_reselectProc->setGeometry(QRect(380, 60, 111, 21));
        DLLInjectorClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(DLLInjectorClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 510, 21));
        DLLInjectorClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(DLLInjectorClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        DLLInjectorClass->setStatusBar(statusBar);

        retranslateUi(DLLInjectorClass);
        QObject::connect(pushButton_reloadProcList, SIGNAL(clicked()), DLLInjectorClass, SLOT(loadProcessList()));
        QObject::connect(pushButton_searchProc, SIGNAL(clicked()), DLLInjectorClass, SLOT(searchProcess()));
        QObject::connect(lineEdit_searchProc, SIGNAL(returnPressed()), pushButton_searchProc, SLOT(click()));
        QObject::connect(pushButton_selectProc, SIGNAL(clicked()), DLLInjectorClass, SLOT(selectProcess()));
        QObject::connect(pushButton_addDLL, SIGNAL(clicked()), DLLInjectorClass, SLOT(addDLL()));
        QObject::connect(pushButton_removeDLL, SIGNAL(clicked()), DLLInjectorClass, SLOT(removeDLL()));
        QObject::connect(pushButton_clearDDLs, SIGNAL(clicked()), DLLInjectorClass, SLOT(clearDLLs()));
        QObject::connect(pushButton_injectDLL, SIGNAL(clicked()), DLLInjectorClass, SLOT(injectDLL()));
        QObject::connect(pushButton_reselectProc, SIGNAL(clicked()), DLLInjectorClass, SLOT(reselectProcess()));

        QMetaObject::connectSlotsByName(DLLInjectorClass);
    } // setupUi

    void retranslateUi(QMainWindow *DLLInjectorClass)
    {
        DLLInjectorClass->setWindowTitle(QApplication::translate("DLLInjectorClass", "DLLInjector", 0));
        label_procList->setText(QApplication::translate("DLLInjectorClass", "Choose process:", 0));
        pushButton_reloadProcList->setText(QApplication::translate("DLLInjectorClass", "Reload Process List", 0));
        pushButton_removeDLL->setText(QApplication::translate("DLLInjectorClass", "Remove DLL", 0));
        pushButton_clearDDLs->setText(QApplication::translate("DLLInjectorClass", "Clear DLLs", 0));
        pushButton_addDLL->setText(QApplication::translate("DLLInjectorClass", "Add DLL", 0));
        pushButton_injectDLL->setText(QApplication::translate("DLLInjectorClass", "Inject", 0));
        pushButton_selectProc->setText(QApplication::translate("DLLInjectorClass", "Select Process", 0));
        lineEdit_searchProc->setPlaceholderText(QApplication::translate("DLLInjectorClass", " Process name", 0));
        pushButton_searchProc->setText(QApplication::translate("DLLInjectorClass", "Search Process", 0));
        pushButton_reselectProc->setText(QApplication::translate("DLLInjectorClass", "Reselect Process", 0));
    } // retranslateUi

};

namespace Ui {
    class DLLInjectorClass: public Ui_DLLInjectorClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DLLINJECTOR_H
