#ifndef DLLINJECTOR_H
#define DLLINJECTOR_H

#include <QtWidgets/QMainWindow>
#include <QFileDialog>
#include <QStandardItemModel>

#include "ui_dllinjector.h"

#include <Windows.h>
#include <Psapi.h>
#include <TlHelp32.h>

#include <vector>
#include <string>
#include <set>

#include "Shlwapi.h"
#pragma comment (lib, "Shlwapi.lib")

class DLLInjector : public QMainWindow
{
	Q_OBJECT

public:
	DLLInjector(QWidget *parent = 0);
	~DLLInjector();

private:
	Ui::DLLInjectorClass ui;
	SYSTEM_INFO sys_info;

	QStandardItemModel *model_listDLLs;

	public slots:

	void loadProcessList();
	void searchProcess();
	void selectProcess();
	void reselectProcess();

	void addDLL();
	void removeDLL();
	void clearDLLs();

	void injectDLL();
};

#endif // DLLINJECTOR_H
