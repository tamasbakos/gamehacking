#include "dllinjector.h"

#define LOADLIBRARYW_32_OFFSET  0x0001a820
#define LOADLIBRARYW_64_OFFSET 	0x00000000000017e0


DLLInjector::DLLInjector(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	model_listDLLs = new QStandardItemModel(0, 1);
	ui.listView_DLLs->setModel(model_listDLLs);

	ui.pushButton_injectDLL->setEnabled(false);
	ui.pushButton_reselectProc->setEnabled(false);

	loadProcessList();
	GetSystemInfo(&sys_info);
}

DLLInjector::~DLLInjector()
{
	delete model_listDLLs;
}

void DLLInjector::loadProcessList()
{
	ui.comboBox_procList->clear();
	bool showUnknownProc = false;

	DWORD bytesReturned;
	DWORD nrProcesses;
	std::vector<DWORD> processIds(1024);

	if (!EnumProcesses(&processIds[0], processIds.size(), &bytesReturned))
	{
		return;
	}

	// Calculate how many process identifiers were returned.
	nrProcesses = bytesReturned / sizeof(DWORD);

	// Put the name and process identifier for each process into the combobox.
	std::vector<QString> pnames;

	for (int i = 0; i < nrProcesses; i++)
	{
		if (processIds[i] != 0)
		{
			// Get a handle to the process.
			HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processIds[i]);

			// Get the process name.
			wchar_t processName[MAX_PATH] = L"<unknown>";

			if (NULL != hProcess)
			{
				HMODULE hMod;
				DWORD cbNeeded;

				if (EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cbNeeded))
				{
					GetModuleBaseNameW(hProcess, hMod, processName, MAX_PATH);
				}
			}

			QString pidstr = QString::number(processIds[i]);
			QString procname = QString::fromWCharArray(processName);

			while (pidstr.length() < 5) pidstr = "0" + pidstr;

			BOOL is32bits;
			IsWow64Process(hProcess, &is32bits);

			QString bits = is32bits ? "[32bit]" : "[64bit]";

			if (procname != "<unknown>" || showUnknownProc)
			{
				QString ins = pidstr + " : " + procname;

				pnames.push_back(procname.toLower() + "|" + ins + "  " + bits);
			}

			CloseHandle(hProcess);
		}
	}

	//ui.comboBox_procList->addItem(QString("<new process>"));

	std::sort(pnames.begin(), pnames.end());

	for (auto pname : pnames)
	{
		ui.comboBox_procList->addItem(pname.split("|")[1]);
	}
}

void DLLInjector::searchProcess()
{
	QString toSearch = ui.lineEdit_searchProc->text().toLower();

	if (toSearch.isEmpty())
	{
		return;
	}

	int currentInd = ui.comboBox_procList->currentIndex();

	for (int i = currentInd + 1; i < ui.comboBox_procList->count(); ++i)
	{
		QString currentName = ui.comboBox_procList->itemText(i).toLower();

		if (currentName.contains(toSearch))
		{
			ui.comboBox_procList->setCurrentIndex(i);
			return;
		}
	}

	for (int i = 0; i < currentInd + 1; ++i)
	{
		QString currentName = ui.comboBox_procList->itemText(i).toLower();

		if (currentName.contains(toSearch))
		{
			ui.comboBox_procList->setCurrentIndex(i);
			return;
		}
	}

	loadProcessList();

	for (int i = 0; i < ui.comboBox_procList->count(); ++i)
	{
		QString currentName = ui.comboBox_procList->itemText(i).toLower();

		if (currentName.contains(toSearch))
		{
			ui.comboBox_procList->setCurrentIndex(i);
			return;
		}
	}
}

void DLLInjector::selectProcess()
{
	QString currentText = ui.pushButton_selectProc->text();

	if (currentText.startsWith("Select"))
	{
		if (model_listDLLs->rowCount() != 0)
		{
			ui.pushButton_injectDLL->setEnabled(true);
		}
		ui.comboBox_procList->setEnabled(false);
		ui.pushButton_reloadProcList->setEnabled(false);
		ui.lineEdit_searchProc->setEnabled(false);
		ui.pushButton_searchProc->setEnabled(false);

		ui.pushButton_reselectProc->setEnabled(true);

		ui.pushButton_selectProc->setText("Unselect Process");
	}
	else if (currentText.startsWith("Unselect"))
	{
		ui.pushButton_injectDLL->setEnabled(false);

		ui.comboBox_procList->setEnabled(true);
		ui.pushButton_reloadProcList->setEnabled(true);
		ui.lineEdit_searchProc->setEnabled(true);
		ui.pushButton_searchProc->setEnabled(true);

		ui.pushButton_reselectProc->setEnabled(false);

		loadProcessList();
		ui.pushButton_selectProc->setText("Select Process");
	}
}

void DLLInjector::reselectProcess()
{
	QString procName = ui.comboBox_procList->currentText().split(":")[1].trimmed().toLower();

	loadProcessList();

	for (int i = 0; i < ui.comboBox_procList->count(); ++i)
	{
		QString currentName = ui.comboBox_procList->itemText(i).toLower();

		if (currentName.contains(procName))
		{
			ui.comboBox_procList->setCurrentIndex(i);
			return;
		}
	}

	selectProcess();
}

void DLLInjector::addDLL()
{
	//QString pathDLL = QFileDialog::getOpenFileName(this, tr("Open DLL"), "C:\\Windows", tr("DLL Files (*.dll)"));
	QString pathDLL = 
		QFileDialog::getOpenFileName(
			this, 
			tr("Open DLL"), "d:\\ProgrammingProjects\\C++\\GameExploitation\\ZumaHookDLL\\Release\\", 
			tr("DLL Files (*.dll)"));
	pathDLL = pathDLL.replace('/', "\\");

	wchar_t relPath[MAX_PATH];
	wchar_t currentDir[MAX_PATH];

	GetCurrentDirectoryW(MAX_PATH, currentDir);

	PathRelativePathToW(relPath, currentDir, FILE_ATTRIBUTE_DIRECTORY, pathDLL.toStdWString().c_str(), FILE_ATTRIBUTE_NORMAL);

	pathDLL = QString::fromStdWString(std::wstring(relPath));
	model_listDLLs->appendRow(new QStandardItem(pathDLL));
	QModelIndex ind = model_listDLLs->index(model_listDLLs->rowCount() - 1, 0);
	ui.listView_DLLs->setCurrentIndex(ind);

	if (!ui.comboBox_procList->isEnabled())
	{
		ui.pushButton_injectDLL->setEnabled(true);
	}
}

void DLLInjector::removeDLL()
{
	QModelIndex index = ui.listView_DLLs->currentIndex();
	int row = index.row();

	if (row == -1)
	{
		return;
	}

	for (int r = row; r < model_listDLLs->rowCount() - 1; ++r)
	{
		model_listDLLs->setItem(r, 0, new QStandardItem(model_listDLLs->item(r + 1, 0)->text()));
	}

	model_listDLLs->removeRows(model_listDLLs->rowCount() - 1, 1);

	if (model_listDLLs->rowCount() == 0)
	{
		ui.pushButton_injectDLL->setEnabled(false);
	}

}

void DLLInjector::clearDLLs()
{
	model_listDLLs->removeRows(0, model_listDLLs->rowCount());

	ui.pushButton_injectDLL->setEnabled(false);
}

void DLLInjector::injectDLL()
{
	QModelIndex index = ui.listView_DLLs->currentIndex();
	int row = index.row();

	if (row == -1)
	{
		return;
	}

	QString dllName = model_listDLLs->item(row)->text();

	std::wstring oldName = dllName.replace(QChar('/'), "\\").toStdWString();
	std::wstring newName = L"__.dll";

	QString proc = ui.comboBox_procList->currentText();
	DWORD pid = proc.split(":")[0].trimmed().toULong();

	HANDLE hRemoteProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);

	if (hRemoteProc == NULL)
	{
		MessageBoxW(NULL, L"Could not open process", L"DLLInjector", MB_OK | MB_ICONINFORMATION);
		return;
	}

	std::vector<HMODULE> modules(1024);
	std::set<std::wstring> modulenames;
	DWORD modulesSize;
	EnumProcessModulesEx(hRemoteProc, &modules[0], modules.size() * sizeof(HMODULE), &modulesSize, LIST_MODULES_ALL);

	for (auto module : modules)
	{
		std::wstring modName;
		modName.resize(MAX_PATH);
		DWORD len = GetModuleBaseNameW(hRemoteProc, module, &modName[0], MAX_PATH);
		modName = modName.substr(0, len);

		modulenames.insert(modName);
	}

	std::vector<BYTE> valuedll(10);

	wcsncpy((wchar_t*)&valuedll[0], L".dll\0", 5);

	MEMORY_BASIC_INFORMATION mem_info;
	std::vector<BYTE> chunk;

	BYTE* addr = (BYTE*)sys_info.lpMinimumApplicationAddress;
	BYTE* remoteAddr = NULL;

	while (addr < sys_info.lpMaximumApplicationAddress)
	{
		VirtualQueryEx(hRemoteProc, addr, &mem_info, sizeof(MEMORY_BASIC_INFORMATION));

		if (mem_info.Protect == PAGE_READWRITE && mem_info.State == MEM_COMMIT)
		{
			chunk.resize(mem_info.RegionSize);

			ReadProcessMemory(hRemoteProc, mem_info.BaseAddress, &chunk[0], mem_info.RegionSize, NULL);

			BYTE* chunkaddr = &chunk[0];

			for (SIZE_T i = 0; i < mem_info.RegionSize; ++i)
			{

				if (memcmp(&valuedll[0], chunkaddr + i, valuedll.size()) == 0)
				{
					wchar_t one = *(wchar_t*)(chunkaddr + i - 4);
					wchar_t two = *(wchar_t*)(chunkaddr + i - 2);

					if ((QChar(one).isDigit() || (QChar('a') <= QChar(one).toLower() && QChar(one).toLower() <= ('z'))) &&
						(QChar(two).isDigit() || (QChar('a') <= QChar(two).toLower() && QChar(two).toLower() <= ('z'))))
					{
						newName[0] = one;
						newName[1] = two;
						remoteAddr = (BYTE*)(mem_info.BaseAddress) + i - 4;

						if (modulenames.find(newName) == modulenames.end()) // if no module with this name yet
						{
							// stop searching
							addr = (BYTE*)sys_info.lpMaximumApplicationAddress; // to break while loop
							break;
						}
					}

				}
			}
		}
		addr += mem_info.RegionSize;
	}

	if (newName == L"__.dll")
	{
		MessageBoxW(NULL, L"Could not find appropriate name to inject dll", L"DLLInjector", MB_OK | MB_ICONINFORMATION);
		return;
	}

	std::wstring newPath;
	newPath.resize(MAX_PATH);

	GetModuleFileNameExW(hRemoteProc, NULL, &newPath[0], MAX_PATH);

	int i = newPath.size() - 1;
	for (; i >= 0 && newPath[i] != '\\'; --i);

	newPath = newPath.substr(0, i + 1);
	std::wstring newDll = newPath + newName; // newPath = path to copy dll to be injected

	CopyFileW(oldName.c_str(), newDll.c_str(), FALSE);
	//SetFileAttributesW(newDll.c_str(), FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_HIDDEN);
	// copied DLL with name from target process next to target process file

	char templ[] = 
		":REPEAT\r\n"
		"timeout /t 5 /nobreak > NUL\r\n"
		"del \"%s\"\r\n"
		"if exist \"%s\" goto REPEAT\r\n"
		"del \"%s\"";
	char *content = new char[4096];
	std::string dllPath = std::string(newDll.begin(), newDll.end());
	std::string batchPath = dllPath.substr(0, dllPath.size() - 3) + "bat";
	sprintf(content, templ, dllPath.c_str(), dllPath.c_str(), batchPath.c_str());

	DWORD len;
	HANDLE delbat = CreateFileA(
		batchPath.c_str(), 
		GENERIC_WRITE, 
		0, 
		NULL, 
		CREATE_ALWAYS, 
		FILE_ATTRIBUTE_NORMAL, // | FILE_ATTRIBUTE_HIDDEN, 
		NULL);
	WriteFile(delbat, content, strlen(content), &len, NULL);
	CloseHandle(delbat);
	delete[] content;

	std::vector<HMODULE> remoteMods(1024);
	DWORD remoteModsSize;
	EnumProcessModulesEx(hRemoteProc, &remoteMods[0], remoteMods.size() * sizeof(HMODULE), &remoteModsSize, LIST_MODULES_ALL);
	HMODULE remoteDllHandle = NULL;

	wchar_t modName[MAX_PATH];
	for (int i = 0; i < remoteModsSize / sizeof(HMODULE); ++i)
	{
		GetModuleBaseNameW(hRemoteProc, remoteMods[i], modName, MAX_PATH);

		if (QString::fromWCharArray(modName).toLower().endsWith("kernel32.dll"))
		{
			remoteDllHandle = remoteMods[i];
			break;
		}
	}


	// check if process 32 or 64 bit
	BOOL is32bits;
	IsWow64Process(hRemoteProc, &is32bits);

	BYTE* loadLibraryRemoteAddr;
	if (is32bits) // target process 32 bit
	{
		loadLibraryRemoteAddr = (BYTE*)((DWORD)remoteDllHandle + LOADLIBRARYW_32_OFFSET);
	}
	else // target process 64 bit
	{
		loadLibraryRemoteAddr = (BYTE*)((DWORD)remoteDllHandle + LOADLIBRARYW_64_OFFSET);
	}

	HANDLE remoteThread = CreateRemoteThread(hRemoteProc, NULL, 0, (LPTHREAD_START_ROUTINE)loadLibraryRemoteAddr, remoteAddr, NULL, 0);

	WaitForSingleObject(remoteThread, INFINITE);


	// Start process to delete dll after exit
	PROCESS_INFORMATION pif;  //Gives info on the thread and process for the new process
	STARTUPINFOA si;          //Defines how to start the program

	ZeroMemory(&si, sizeof(si)); //Zero the STARTUPINFO struct
	si.cb = sizeof(si);         //Must set size of structure
	BOOL bRet = CreateProcessA(
		batchPath.c_str(), //Path to executable file
		NULL,   //Command string - not needed here
		NULL,   //Process handle not inherited
		NULL,   //Thread handle not inherited
		FALSE,  //No inheritance of handles
		CREATE_NO_WINDOW,      //Dont show window
		NULL,   //Same environment block as this prog
		NULL,   //Current directory - no separate path
		&si,    //Pointer to STARTUPINFO
		&pif);   //Pointer to PROCESS_INFORMATION

	CloseHandle(hRemoteProc);
}