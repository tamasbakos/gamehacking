#include "windebugger.h"

#pragma comment(lib,"ntdll.lib")

WinDebugger::WinDebugger(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	// list of debug events
	model_debugEvents = new QStandardItemModel(0, 1);
	ui.listView_debugEvents->setModel(model_debugEvents);

	// Threads table
	model_threads = new QStandardItemModel(0, 2);
	ui.tableView_threads->setModel(model_threads);

	model_threads->setHorizontalHeaderItem(0, new QStandardItem(QString("ID")));
	model_threads->setHorizontalHeaderItem(1, new QStandardItem(QString("Suspend count")));

	double width = ui.tableView_threads->width() - 18;
	ui.tableView_threads->setColumnWidth(0, 0.5*width);
	ui.tableView_threads->setColumnWidth(1, 0.5*width);

	// Disassembled code table
	model_asm = new QStandardItemModel(0, 3);
	ui.tableView_asm->setModel(model_asm);

	model_asm->setHorizontalHeaderItem(0, new QStandardItem(QString("Address")));
	model_asm->setHorizontalHeaderItem(1, new QStandardItem(QString("Mnemonic")));
	model_asm->setHorizontalHeaderItem(2, new QStandardItem(QString("Operands")));

	width = ui.tableView_asm->width() - 18;
	ui.tableView_asm->setColumnWidth(0, 0.28*width);
	ui.tableView_asm->setColumnWidth(1, 0.20*width);
	ui.tableView_asm->setColumnWidth(2, 0.33*width);

	// Code caves list
	model_codeCaves = new QStandardItemModel();
	//ui.listView_codeCaves->setModel(model_codeCaves);

	dwProcessId = -1;
	debugging = false;
	newpath = "";

	loadProcessList();

	ui.pushButton_detachProc->setHidden(true);
	ui.pushButton_killProc->setHidden(true);

	GetSystemInfo(&sys_info);
}

WinDebugger::~WinDebugger()
{
	if (dwProcessId != -1)
	{
		DebugActiveProcessStop(dwProcessId);
	}
}

void WinDebugger::loadProcessList()
{
	ui.comboBox_procList->clear();
	bool showUnknownProc = false;

	DWORD bytesReturned;
	DWORD nrProcesses;
	std::vector<DWORD> processIds(1024);

	if (!EnumProcesses(&processIds[0], processIds.size(), &bytesReturned))
	{
		return;
	}

	// Calculate how many process identifiers were returned.
	nrProcesses = bytesReturned / sizeof(DWORD);

	// Put the name and process identifier for each process into the combobox.
	std::vector<QString> pnames;

	for (int i = 0; i < nrProcesses; i++)
	{
		if (processIds[i] != 0)
		{
			// Get a handle to the process.
			HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processIds[i]);

			// Get the process name.
			wchar_t processName[MAX_PATH] = L"<unknown>";

			if (NULL != hProcess)
			{
				HMODULE hMod;
				DWORD cbNeeded;

				if (EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cbNeeded))
				{
					GetModuleBaseNameW(hProcess, hMod, processName, MAX_PATH);
				}
			}

			QString pidstr = QString::number(processIds[i]);
			QString procname = QString::fromWCharArray(processName);

			while (pidstr.length() < 5) pidstr = "0" + pidstr;

			BOOL is32bits;
			IsWow64Process(hProcess, &is32bits);

			QString bits = is32bits ? "[32bit]" : "[64bit]";

			if (procname != "<unknown>" || showUnknownProc)
			{
				QString ins = pidstr + " : " + procname + "  ";

				pnames.push_back(procname.toLower() + "|" + ins + "  " + bits);
			}

			CloseHandle(hProcess);
		}
	}

	ui.comboBox_procList->addItem(QString("<new process>"));

	std::sort(pnames.begin(), pnames.end());

	for (auto pname : pnames)
	{
		ui.comboBox_procList->addItem(pname.split("|")[1]);
	}
}

void WinDebugger::disassemble()
{
	model_asm->removeRows(0, model_asm->rowCount());

	CX86Disasm64 disasm;

	if (disasm.GetError())
		return;

	disasm.SetDetail(cs_opt_value::CS_OPT_OFF);
	disasm.SetSyntax(cs_opt_value::CS_OPT_SYNTAX_INTEL);

	MEMORY_BASIC_INFORMATION mem_info;
	std::vector<BYTE> codechunk;

	BYTE* addr = (BYTE*)sys_info.lpMinimumApplicationAddress;

	std::vector<BYTE*> caves;

	while (addr < sys_info.lpMaximumApplicationAddress)
	{
		VirtualQueryEx(pi.hProcess, addr, &mem_info, sizeof(MEMORY_BASIC_INFORMATION));

		if (mem_info.Protect == PAGE_READWRITE && mem_info.State == MEM_COMMIT)
		{
			codechunk.resize(mem_info.RegionSize);

			BYTE* chunkaddr = &codechunk[0];
			ReadProcessMemory(pi.hProcess, mem_info.BaseAddress, chunkaddr, mem_info.RegionSize, NULL);

			//
			size_t i, j;
			for (i = 0; i < mem_info.RegionSize;)
			{
				BYTE current = codechunk[i];

				for (j = i + 1; j < mem_info.RegionSize; ++j)
				{
					if (current != codechunk[j])
					{
						break;
					}
				}

				if (j - i > 4096)
				{
					caves.push_back(addr + i);

					QString address = QString::number((size_t)(addr + i), 16);
					model_codeCaves->appendRow(new QStandardItem("0x" + address.toUpper()));
				}
				i = j;
			}

			// Disassemble here

			auto insn = disasm.Disasm(chunkaddr, codechunk.size(), (size_t)addr);

			for (size_t i = 0; i < insn->Count; i++)
			{
				model_asm->appendRow(new QStandardItem(1, 3));

				QString address = QString::number(insn->Instructions(i)->address, 16);
				model_asm->setItem(model_asm->rowCount() - 1, 0, new QStandardItem("0x" + address.toUpper()));

				QString mnemonic = QString(insn->Instructions(i)->mnemonic);
				model_asm->setItem(model_asm->rowCount() - 1, 1, new QStandardItem(mnemonic));

				QString opstr = QString(insn->Instructions(i)->op_str);
				model_asm->setItem(model_asm->rowCount() - 1, 2, new QStandardItem(opstr));

			}

			//model_asm->appendRow(new QStandardItem(1, 3));

		}

		addr += mem_info.RegionSize;
	} // finished searching for the memory of the process



}

void WinDebugger::gotoCodeCave(QModelIndex index)
{
	for (int i = 0; i < model_asm->rowCount() - 1; ++i)
	{
		size_t left = model_asm->item(i)->text().toLongLong(NULL, 16);
		size_t right = model_asm->item(i + 1)->text().toLongLong(NULL, 16);

		size_t caveAddr = model_codeCaves->item(index.row())->text().toLongLong(NULL, 16);

		if (left <= caveAddr && caveAddr < right)
		{
			QModelIndex ind = model_asm->index(i, 0);
			ui.tableView_asm->setCurrentIndex(ind);
			break;
		}
	}
}

void WinDebugger::pauseresumeDebugging()
{
	if (!debugging)
	{
		debugging = true;
		ResumeThread(hDebuggingThread);
	}
	else
	{
		SuspendThread(hDebuggingThread);
		debugging = false;
	}
}


void WinDebugger::opendebugProcess() // to be called by pressing
{
	QString proc = ui.comboBox_procList->currentText();

	if (proc.startsWith("<new")) // must be done in GUI thread
	{
		//QString path = QFileDialog::getOpenFileName(this, tr("Open file"), "C:\\Windows", tr("EXE Files (*.exe)"));
		newpath = QFileDialog::getOpenFileName(this, tr("Open file"), "D:\\ProgrammingProjects\\C++\\GameExploitation\\DebugThis\\Debug", tr("EXE Files (*.exe)"));

		if (newpath == "")
			return;
	}

	debugging = true;

	hDebuggingThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)debug, this, 0, NULL);


	ui.comboBox_procList->setEnabled(false);
	ui.pushButton_reloadProcList->setEnabled(false);

	ui.pushButton_debugProc->setHidden(true);
	ui.pushButton_detachProc->setHidden(false);
	ui.pushButton_killProc->setHidden(false);

	// later
	ui.pushButton_listThreads->setEnabled(true);
}

void WinDebugger::detachProcess()
{
	debugging = false;
	Sleep(100);

	DebugActiveProcessStop(dwProcessId);
	CloseHandle(pi.hProcess);
	dwProcessId = -1;

	ui.comboBox_procList->setEnabled(true);
	ui.pushButton_reloadProcList->setEnabled(true);

	ui.pushButton_debugProc->setHidden(false);
	ui.pushButton_detachProc->setHidden(true);
	ui.pushButton_killProc->setHidden(true);

	loadProcessList();

	//later
	ui.pushButton_listThreads->setEnabled(false);
}

void WinDebugger::killProcess()
{
	debugging = false;
	Sleep(100);

	TerminateProcess(pi.hProcess, 0);
	//CloseHandle(pi.hProcess); // crashes with invalid handle

	dwProcessId = -1;

	ui.comboBox_procList->setEnabled(true);
	ui.pushButton_reloadProcList->setEnabled(true);

	ui.pushButton_debugProc->setHidden(false);
	ui.pushButton_detachProc->setHidden(true);
	ui.pushButton_killProc->setHidden(true);

	loadProcessList();

	//later
	ui.pushButton_listThreads->setEnabled(false);
}

/*static*/ void WinDebugger::debug(void* param)
{
	WinDebugger* This = (WinDebugger*)param;

	// start debugging selected or new process
	QString proc = This->ui.comboBox_procList->currentText();

	if (proc.startsWith("<new")) // create new process for debugging
	{
		// TODO: option for breakpoint at entry point

		This->si = { 0 };
		This->si.cb = sizeof(STARTUPINFO);
		This->pi = { 0 };

		CreateProcessW(
			This->newpath.toStdWString().c_str(),
			NULL, NULL, NULL,
			FALSE,
			DEBUG_ONLY_THIS_PROCESS,
			NULL, NULL,
			&(This->si),
			&(This->pi));
		QStringList l = This->newpath.split("/");

		This->dwProcessId = This->pi.dwProcessId;
		QString pidstr = QString::number(This->dwProcessId);
		QString procname = l[l.length() - 1];

		while (pidstr.length() < 5) pidstr = "0" + pidstr;

		BOOL is32bits;
		IsWow64Process(This->pi.hProcess, &is32bits);

		QString bits = is32bits ? "[32bit]" : "[64bit]";

		This->ui.comboBox_procList->addItem(pidstr + " : " + procname + "  " + bits);
		This->ui.comboBox_procList->setCurrentIndex(This->ui.comboBox_procList->count() - 1);
	}
	else // attach to existing process for debugging
	{
		QString pidstr = proc.split(":")[0].trimmed();
		This->dwProcessId = pidstr.toULong();

		if (!DebugActiveProcess(This->dwProcessId))
		{
			This->dwProcessId = -1;
			return; // unsuccessful open
		}

		This->pi.hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, This->dwProcessId);

	}

	DebugSetProcessKillOnExit(FALSE); // do not kill debugee when exiting debugger
	//This->disassemble();


	DEBUG_EVENT debug_event;
	QString debugMessage;

	This->model_debugEvents->removeRows(0, This->model_debugEvents->rowCount());

	// debugging loop
	while (This->debugging)
	{
		This->dwDbgContinueStatus = DBG_CONTINUE;

		if (!WaitForDebugEvent(&debug_event, INFINITE))
			return;

		debugMessage = This->processDebugEvent(&debug_event);

		This->model_debugEvents->appendRow(new QStandardItem(debugMessage));

		ContinueDebugEvent(debug_event.dwProcessId, debug_event.dwThreadId, This->dwDbgContinueStatus);
	}

	// Debugging was quit by the user

}

QString WinDebugger::processDebugEvent(DEBUG_EVENT *debug_event)
{
	QString retDebugMsg;

	switch (debug_event->dwDebugEventCode)
	{
	case OUTPUT_DEBUG_STRING_EVENT:
	{
		retDebugMsg = "Debug Output: ";
		OUTPUT_DEBUG_STRING_INFO *DebugString = &debug_event->u.DebugString;

		size_t charsize = DebugString->fUnicode ? 2 : 1;
		BYTE* msgBytes = new BYTE[DebugString->nDebugStringLength * charsize];

		ReadProcessMemory(
			pi.hProcess,
			DebugString->lpDebugStringData,
			msgBytes,
			DebugString->nDebugStringLength * charsize,
			NULL);

		retDebugMsg +=
			DebugString->fUnicode ?
			QString::fromWCharArray((wchar_t*)msgBytes) : QString::fromUtf8((char*)msgBytes);

		delete[]msgBytes;
	}
	break;

	case CREATE_PROCESS_DEBUG_EVENT:
	{
		retDebugMsg = "Process created ";
		CREATE_PROCESS_DEBUG_INFO *CreateProcInfo = &debug_event->u.CreateProcessInfo;
		wchar_t* filename = new wchar_t[MAX_PATH];

		GetFinalPathNameByHandleW(
			CreateProcInfo->hFile,
			filename,
			MAX_PATH,
			0);

		retDebugMsg += QString::fromWCharArray(filename).mid(4);

		retDebugMsg += " at address: 0x";
		retDebugMsg += QString::number((QWORD)CreateProcInfo->lpStartAddress, 16);

		delete[]filename;
	}
	break;

	case EXIT_PROCESS_DEBUG_EVENT:
	{
		retDebugMsg = "Process exited with code ";
		EXIT_PROCESS_DEBUG_INFO *ExitProcInfo = &debug_event->u.ExitProcess;

		retDebugMsg += QString::number(ExitProcInfo->dwExitCode);
		retDebugMsg += " (0x" + QString::number(ExitProcInfo->dwExitCode, 16) + ")";

		debugging = false;

		ui.pushButton_killProc->click();
	}
	break;

	case LOAD_DLL_DEBUG_EVENT:
	{
		retDebugMsg = "DLL loaded ";
		LOAD_DLL_DEBUG_INFO *LoadDllInfo = &debug_event->u.LoadDll;
		wchar_t* filename = new wchar_t[MAX_PATH];

		GetFinalPathNameByHandleW(
			LoadDllInfo->hFile,
			filename,
			MAX_PATH,
			0);

		QString DllName = QString::fromWCharArray(filename).mid(4);
		DllNameMap[LoadDllInfo->lpBaseOfDll] = DllName;

		retDebugMsg += DllName;

		delete[]filename;
	}
	break;

	case UNLOAD_DLL_DEBUG_EVENT: // only FreeLibrary calls
	{
		retDebugMsg = "DLL unloaded ";
		UNLOAD_DLL_DEBUG_INFO *UnloadDllInfo = &debug_event->u.UnloadDll;

		retDebugMsg += DllNameMap[UnloadDllInfo->lpBaseOfDll];
	}
	break;

	case CREATE_THREAD_DEBUG_EVENT: // not received for main thred
	{
		retDebugMsg = "Thread 0x" + QString::number(debug_event->dwThreadId, 16) + " created at 0x";
		CREATE_THREAD_DEBUG_INFO *CreateThreadInfo = &debug_event->u.CreateThread;

		retDebugMsg += QString::number((QWORD)CreateThreadInfo->lpStartAddress, 16);
	}
	break;

	case EXIT_THREAD_DEBUG_EVENT:
	{
		retDebugMsg = "Thread 0x" + QString::number(debug_event->dwThreadId, 16) + " exited with code ";
		EXIT_THREAD_DEBUG_INFO *ExitThreadInfo = &debug_event->u.ExitThread;

		retDebugMsg += QString::number((DWORD)ExitThreadInfo->dwExitCode);
		retDebugMsg += " (0x" + QString::number((DWORD)ExitThreadInfo->dwExitCode) + ")";
	}
	break;

	case EXCEPTION_DEBUG_EVENT:
	{
		EXCEPTION_DEBUG_INFO *Exception = &debug_event->u.Exception;

		switch (Exception->ExceptionRecord.ExceptionCode)
		{
		case EXCEPTION_BREAKPOINT:
		{
			retDebugMsg = "Breakpoint hit at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		break;

		case EXCEPTION_ACCESS_VIOLATION:
		{
			retDebugMsg = "Access violation exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:
		{
			retDebugMsg = "Out of array bounds exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_DATATYPE_MISALIGNMENT:
		{
			retDebugMsg = "Datatype misalignment exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_FLT_DENORMAL_OPERAND:
		{
			retDebugMsg = "Denormal floating-point value exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_FLT_DIVIDE_BY_ZERO:
		{
			retDebugMsg = "Division by floating-point zero exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_FLT_INEXACT_RESULT:
		{
			retDebugMsg = "Inexact floating-point result exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_FLT_INVALID_OPERATION:
		{
			retDebugMsg = "Invalid floating-point operation exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_FLT_OVERFLOW:
		{
			retDebugMsg = "Floating-point value overflow exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_FLT_STACK_CHECK:
		{
			retDebugMsg = "Stack overfloat due to floating-point operation exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_FLT_UNDERFLOW:
		{
			retDebugMsg = "Floating-point value underflow exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_ILLEGAL_INSTRUCTION:
		{
			retDebugMsg = "Illegal instruction exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_IN_PAGE_ERROR:
		{
			retDebugMsg = "Memory page exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_INT_DIVIDE_BY_ZERO:
		{
			retDebugMsg = "Division by integer zero exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_INT_OVERFLOW:
		{
			retDebugMsg = "Integer overflow exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_INVALID_DISPOSITION:
		{
			retDebugMsg = "Invalid disposition exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_NONCONTINUABLE_EXCEPTION:
		{
			retDebugMsg = "Non continuable exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_PRIV_INSTRUCTION:
		{
			retDebugMsg = "Private instruction exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_SINGLE_STEP:
		{
			retDebugMsg = "Single step exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}
		case EXCEPTION_STACK_OVERFLOW:
		{
			retDebugMsg = "Stack overflow exception at address 0x";
			retDebugMsg += QString::number((DWORD)Exception->ExceptionRecord.ExceptionAddress, 16);
		}

		default:
		{
			retDebugMsg = "Exception";
		}
		break;

		}

		if (Exception->ExceptionRecord.ExceptionCode != EXCEPTION_BREAKPOINT)
		{
			if (Exception->dwFirstChance == 1)
			{
				retDebugMsg = "First chance " + retDebugMsg;
			}

			dwDbgContinueStatus = DBG_EXCEPTION_NOT_HANDLED;
		}
	}
	break;

	default:
		break;
	}

	return retDebugMsg;
}

void WinDebugger::listThreads()
{
	QModelIndex index = ui.tableView_threads->currentIndex();
	model_threads->removeRows(0, model_threads->rowCount());

	/*
	HANDLE hThreadSnap = INVALID_HANDLE_VALUE;
	THREADENTRY32 te32;

	// Take a snapshot of all running threads
	hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	if (hThreadSnap == INVALID_HANDLE_VALUE)
	{
	return;
	}

	// Fill in the size of the structure before using it.
	te32.dwSize = sizeof(THREADENTRY32);

	// Retrieve information about the first thread,
	// and exit if unsuccessful
	if (!Thread32First(hThreadSnap, &te32))
	{
	CloseHandle(hThreadSnap);     // Must clean up the snapshot object!
	return;
	}

	// Now walk the thread list of the system,
	// and display information about each thread
	// associated with the specified process
	do
	{
	if (te32.th32OwnerProcessID == dwProcessId)
	{
	model_threads->appendRow(new QStandardItem(1, 2));

	QString strThreadId = "0x" + QString::number(te32.th32ThreadID, 16).toUpper();

	HANDLE hThread = OpenThread(THREAD_ALL_ACCESS, FALSE, te32.th32ThreadID);

	QString strSuspendCount = "";
	DWORD dwSuspendCount;

	dwSuspendCount = SuspendThread(hThread);
	ResumeThread(hThread);
	strSuspendCount += QString::number(dwSuspendCount);
	strSuspendCount += dwSuspendCount == 0 ? " (running)" : " (suspended)";

	CloseHandle(hThread);
	model_threads->setItem(model_threads->rowCount() - 1, 0, new QStandardItem(strThreadId));
	model_threads->setItem(model_threads->rowCount() - 1, 1, new QStandardItem(strSuspendCount));
	}
	} while (Thread32Next(hThreadSnap, &te32));

	//  Clean up the snapshot object.
	CloseHandle(hThreadSnap);
	*/

	DWORD dwSpiSize = 0;

	SYSTEM_PROCESS_INFO* pSysProcInfo = NULL;
	SYSTEM_PROCESS_INFO* pIterSpi = NULL;
	SYSTEM_PROCESS_INFO* pCurrentSysProcInfo = NULL;

	// get size of SPI array 
	NTSTATUS ntRet = NtQuerySystemInformation(
		SystemProcessInformation,
		NULL,
		0,
		&dwSpiSize);

	if (ntRet != STATUS_INFO_LENGTH_MISMATCH)
	{
		return;
	}

	pSysProcInfo = (SYSTEM_PROCESS_INFO*)(new BYTE[dwSpiSize]);

	ntRet = NtQuerySystemInformation(
		SystemProcessInformation,
		pSysProcInfo,
		dwSpiSize,
		&dwSpiSize);

	pIterSpi = pSysProcInfo;

	while (true)
	{
		if ((DWORD)pIterSpi->ProcessId == dwProcessId)
		{
			pCurrentSysProcInfo = pIterSpi;
			break;
		}

		if (pIterSpi->NextEntryOffset == 0)
		{
			return;
		}

		pIterSpi = (SYSTEM_PROCESS_INFO*)((BYTE*)pIterSpi + pIterSpi->NextEntryOffset);

	}

	QString strThreadId = "";
	QString strSuspendCount = "";
	SYSTEM_THREAD* pSysThreadInfo = NULL;

	for (int t = 0; t < pCurrentSysProcInfo->NumberOfThreads; ++t)
	{
		pSysThreadInfo = &pCurrentSysProcInfo->Threads[t];

		strThreadId = QString::number((QWORD)pSysThreadInfo->ClientId.UniqueThread, 16);

		if (pSysThreadInfo->WaitReason == 5)
		{
			HANDLE hThread = OpenThread(THREAD_ALL_ACCESS, FALSE, (QWORD)pSysThreadInfo->ClientId.UniqueThread);

			DWORD dwSuspendCount;

			dwSuspendCount = SuspendThread(hThread);
			ResumeThread(hThread);
			strSuspendCount = QString::number(dwSuspendCount) + " (suspended)";

			CloseHandle(hThread);
		}
		else
		{
			strSuspendCount = "0 (running)";
		}

		model_threads->appendRow(new QStandardItem(1, 2));
		model_threads->setItem(model_threads->rowCount() - 1, 0, new QStandardItem(strThreadId));
		model_threads->setItem(model_threads->rowCount() - 1, 1, new QStandardItem(strSuspendCount));
	}

	delete[](BYTE*)pSysProcInfo;

	ui.tableView_threads->setCurrentIndex(index);
}

void WinDebugger::suspendThread()
{
	QModelIndex index = ui.tableView_threads->currentIndex();

	if (index.row() < 0 || index.column() < 0)
	{
		return;
	}

	QString s = model_threads->item(index.row(), 0)->text();
	DWORD thID = s.split(" ")[0].toInt(NULL, 16);

	HANDLE hThread = OpenThread(THREAD_ALL_ACCESS, FALSE, thID);

	DWORD ret = SuspendThread(hThread);

	CloseHandle(hThread);

	listThreads();
}

void WinDebugger::resumeThread()
{
	QModelIndex index = ui.tableView_threads->currentIndex();

	if (index.row() < 0 || index.column() < 0)
	{
		return;
	}

	QString s = model_threads->item(index.row(), 0)->text();
	DWORD thID = s.split(" ")[0].toInt(NULL, 16);

	HANDLE hThread = OpenThread(THREAD_ALL_ACCESS, FALSE, thID);

	DWORD ret = ResumeThread(hThread);

	CloseHandle(hThread);

	listThreads();
}