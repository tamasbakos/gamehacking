/********************************************************************************
** Form generated from reading UI file 'windebugger.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDEBUGGER_H
#define UI_WINDEBUGGER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WinDebuggerClass
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton_debugProc;
    QComboBox *comboBox_procList;
    QLabel *label_procList;
    QPushButton *pushButton_reloadProcList;
    QListView *listView_debugEvents;
    QPushButton *pushButton_listen;
    QPushButton *pushButton_listThreads;
    QPushButton *pushButton_suspend;
    QPushButton *pushButton_resume;
    QPushButton *pushButton_detachProc;
    QPushButton *pushButton_killProc;
    QTableView *tableView_threads;
    QTableView *tableView_asm;
    QLabel *label_debugEvents;
    QLabel *label_threads;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *WinDebuggerClass)
    {
        if (WinDebuggerClass->objectName().isEmpty())
            WinDebuggerClass->setObjectName(QStringLiteral("WinDebuggerClass"));
        WinDebuggerClass->resize(731, 609);
        centralWidget = new QWidget(WinDebuggerClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton_debugProc = new QPushButton(centralWidget);
        pushButton_debugProc->setObjectName(QStringLiteral("pushButton_debugProc"));
        pushButton_debugProc->setGeometry(QRect(540, 30, 131, 23));
        comboBox_procList = new QComboBox(centralWidget);
        comboBox_procList->setObjectName(QStringLiteral("comboBox_procList"));
        comboBox_procList->setGeometry(QRect(20, 30, 371, 22));
        label_procList = new QLabel(centralWidget);
        label_procList->setObjectName(QStringLiteral("label_procList"));
        label_procList->setGeometry(QRect(30, 5, 111, 21));
        pushButton_reloadProcList = new QPushButton(centralWidget);
        pushButton_reloadProcList->setObjectName(QStringLiteral("pushButton_reloadProcList"));
        pushButton_reloadProcList->setGeometry(QRect(410, 30, 111, 23));
        listView_debugEvents = new QListView(centralWidget);
        listView_debugEvents->setObjectName(QStringLiteral("listView_debugEvents"));
        listView_debugEvents->setGeometry(QRect(20, 400, 681, 111));
        pushButton_listen = new QPushButton(centralWidget);
        pushButton_listen->setObjectName(QStringLiteral("pushButton_listen"));
        pushButton_listen->setGeometry(QRect(20, 350, 191, 21));
        pushButton_listThreads = new QPushButton(centralWidget);
        pushButton_listThreads->setObjectName(QStringLiteral("pushButton_listThreads"));
        pushButton_listThreads->setEnabled(false);
        pushButton_listThreads->setGeometry(QRect(400, 350, 75, 23));
        pushButton_suspend = new QPushButton(centralWidget);
        pushButton_suspend->setObjectName(QStringLiteral("pushButton_suspend"));
        pushButton_suspend->setGeometry(QRect(490, 350, 75, 23));
        pushButton_resume = new QPushButton(centralWidget);
        pushButton_resume->setObjectName(QStringLiteral("pushButton_resume"));
        pushButton_resume->setGeometry(QRect(580, 350, 75, 23));
        pushButton_detachProc = new QPushButton(centralWidget);
        pushButton_detachProc->setObjectName(QStringLiteral("pushButton_detachProc"));
        pushButton_detachProc->setGeometry(QRect(540, 30, 61, 23));
        pushButton_killProc = new QPushButton(centralWidget);
        pushButton_killProc->setObjectName(QStringLiteral("pushButton_killProc"));
        pushButton_killProc->setGeometry(QRect(610, 30, 61, 23));
        tableView_threads = new QTableView(centralWidget);
        tableView_threads->setObjectName(QStringLiteral("tableView_threads"));
        tableView_threads->setGeometry(QRect(400, 230, 291, 111));
        tableView_threads->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView_threads->setSelectionMode(QAbstractItemView::SingleSelection);
        tableView_threads->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableView_threads->verticalHeader()->setVisible(false);
        tableView_threads->verticalHeader()->setDefaultSectionSize(20);
        tableView_asm = new QTableView(centralWidget);
        tableView_asm->setObjectName(QStringLiteral("tableView_asm"));
        tableView_asm->setGeometry(QRect(20, 60, 371, 281));
        tableView_asm->setSelectionMode(QAbstractItemView::SingleSelection);
        tableView_asm->setSelectionBehavior(QAbstractItemView::SelectRows);
        label_debugEvents = new QLabel(centralWidget);
        label_debugEvents->setObjectName(QStringLiteral("label_debugEvents"));
        label_debugEvents->setGeometry(QRect(30, 380, 91, 16));
        label_threads = new QLabel(centralWidget);
        label_threads->setObjectName(QStringLiteral("label_threads"));
        label_threads->setGeometry(QRect(410, 210, 71, 16));
        WinDebuggerClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(WinDebuggerClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 731, 21));
        WinDebuggerClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(WinDebuggerClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        WinDebuggerClass->setStatusBar(statusBar);

        retranslateUi(WinDebuggerClass);
        QObject::connect(pushButton_reloadProcList, SIGNAL(clicked()), WinDebuggerClass, SLOT(loadProcessList()));
        QObject::connect(pushButton_listThreads, SIGNAL(clicked()), WinDebuggerClass, SLOT(listThreads()));
        QObject::connect(pushButton_suspend, SIGNAL(clicked()), WinDebuggerClass, SLOT(suspendThread()));
        QObject::connect(pushButton_resume, SIGNAL(clicked()), WinDebuggerClass, SLOT(resumeThread()));
        QObject::connect(pushButton_debugProc, SIGNAL(clicked()), WinDebuggerClass, SLOT(opendebugProcess()));
        QObject::connect(pushButton_detachProc, SIGNAL(clicked()), WinDebuggerClass, SLOT(detachProcess()));
        QObject::connect(pushButton_killProc, SIGNAL(clicked()), WinDebuggerClass, SLOT(killProcess()));

        QMetaObject::connectSlotsByName(WinDebuggerClass);
    } // setupUi

    void retranslateUi(QMainWindow *WinDebuggerClass)
    {
        WinDebuggerClass->setWindowTitle(QApplication::translate("WinDebuggerClass", "WinDebugger", 0));
        pushButton_debugProc->setText(QApplication::translate("WinDebuggerClass", "Open and Debug Process", 0));
        label_procList->setText(QApplication::translate("WinDebuggerClass", "Select process:", 0));
        pushButton_reloadProcList->setText(QApplication::translate("WinDebuggerClass", "Reload Process List", 0));
        pushButton_listen->setText(QApplication::translate("WinDebuggerClass", "Resume Listening to Debug Events", 0));
        pushButton_listThreads->setText(QApplication::translate("WinDebuggerClass", "List Threads", 0));
        pushButton_suspend->setText(QApplication::translate("WinDebuggerClass", "Suspend", 0));
        pushButton_resume->setText(QApplication::translate("WinDebuggerClass", "Resume", 0));
        pushButton_detachProc->setText(QApplication::translate("WinDebuggerClass", "Detach", 0));
        pushButton_killProc->setText(QApplication::translate("WinDebuggerClass", "Kill", 0));
        label_debugEvents->setText(QApplication::translate("WinDebuggerClass", "Debug Events:", 0));
        label_threads->setText(QApplication::translate("WinDebuggerClass", "Threads:", 0));
    } // retranslateUi

};

namespace Ui {
    class WinDebuggerClass: public Ui_WinDebuggerClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDEBUGGER_H
