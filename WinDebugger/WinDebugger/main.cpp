#include "windebugger.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	WinDebugger w;
	w.show();
	return a.exec();
}
