#ifndef WINDEBUGGER_H
#define WINDEBUGGER_H

#include <QtWidgets/QMainWindow>
#include <QFileDialog>
#include <QStandardItemModel>

#include "ui_windebugger.h"
#include <Windows.h>
#include <Psapi.h>
#include <tlhelp32.h>
#include <winternl.h>

#include <vector>
#include <utility>

#include <../cccapstone/cppbindings/X86Disasm.hh>

#include "internals.h"

class WinDebugger : public QMainWindow
{
	Q_OBJECT

public:
	WinDebugger(QWidget *parent = 0);
	~WinDebugger();

private:
	Ui::WinDebuggerClass ui;

	DWORD dwProcessId;

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	SYSTEM_INFO sys_info;

	std::map <LPVOID, QString> DllNameMap;

	QStandardItemModel *model_debugEvents;
	QStandardItemModel *model_threads;
	QStandardItemModel *model_asm;
	QStandardItemModel *model_codeCaves;

	HANDLE hDebuggingThread;

	static void debug(void* param);
	bool debugging;
	DWORD dwDbgContinueStatus;

	QString newpath;

	void disassemble();
	QString processDebugEvent(DEBUG_EVENT *debug_event);

public slots:

	void loadProcessList();

	void opendebugProcess();

	void detachProcess();
	void killProcess();

	void gotoCodeCave(QModelIndex index);

	void pauseresumeDebugging();

	void listThreads();

	void suspendThread();
	void resumeThread();

};

#endif // WINDEBUGGER_H
