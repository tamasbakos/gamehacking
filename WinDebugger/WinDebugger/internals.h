#ifndef INTERNALS_H
#define INTERNALS_H

#include <windows.h>

#define STATUS_INFO_LENGTH_MISMATCH 0xC0000004

typedef DWORDLONG QWORD;

typedef LONG KPRIORITY;

typedef struct _CLIENT_ID {
	HANDLE          UniqueProcess;
	HANDLE          UniqueThread;
} CLIENT_ID;

enum THREAD_STATE {
	StateInitialized,
	StateReady,
	StateRunning,
	StateStandby,
	StateTerminated,
	StateWait,
	StateTransition,
	StateUnknown
}; // enum THREAD_STATE

typedef struct _SYSTEM_THREAD {
	LARGE_INTEGER  KernelTime;
	LARGE_INTEGER  UserTime;
	LARGE_INTEGER  CreateTime;
	ULONG          WaitTime;
	PVOID          StartAddress;
	CLIENT_ID      ClientId;
	KPRIORITY      Priority;
	KPRIORITY      BasePriority;
	ULONG          ContextSwitchCount;
	THREAD_STATE   State;
	ULONG           WaitReason;
} SYSTEM_THREAD, *PSYSTEM_THREAD;

typedef NTSTATUS(WINAPI *tNTQSI)(
	ULONG SystemInformationClass,
	PVOID SystemInformation,
	ULONG SystemInformationLength,
	PULONG ReturnLength
	);

typedef struct _VM_COUNTERS {
#ifdef _WIN64
	SIZE_T         PeakVirtualSize;
	SIZE_T         PageFaultCount;
	SIZE_T         PeakWorkingSetSize;
	SIZE_T         WorkingSetSize;
	SIZE_T         QuotaPeakPagedPoolUsage;
	SIZE_T         QuotaPagedPoolUsage;
	SIZE_T         QuotaPeakNonPagedPoolUsage;
	SIZE_T         QuotaNonPagedPoolUsage;
	SIZE_T         PagefileUsage;
	SIZE_T         PeakPagefileUsage;
	SIZE_T         VirtualSize;
#else
	SIZE_T         PeakVirtualSize;
	SIZE_T         VirtualSize;
	ULONG          PageFaultCount;
	SIZE_T         PeakWorkingSetSize;
	SIZE_T         WorkingSetSize;
	SIZE_T         QuotaPeakPagedPoolUsage;
	SIZE_T         QuotaPagedPoolUsage;
	SIZE_T         QuotaPeakNonPagedPoolUsage;
	SIZE_T         QuotaNonPagedPoolUsage;
	SIZE_T         PagefileUsage;
	SIZE_T         PeakPagefileUsage;
#endif
} VM_COUNTERS;

typedef struct _SYSTEM_PROCESS_INFO
{
	DWORD            NextEntryOffset;
	DWORD            NumberOfThreads;
	DWORD            Reserved1[6];
	LARGE_INTEGER    CreateTime;
	LARGE_INTEGER    UserTime;
	LARGE_INTEGER    KernelTime;
	UNICODE_STRING   ProcessName;
	KPRIORITY        BasePriority;
	HANDLE           ProcessId;
	HANDLE           ParentProcessId;
	DWORD            HandleCount;
	DWORD            Reserved2[2];
	VM_COUNTERS      VmCounters;
	DWORD			 PrivatePageCount;
	IO_COUNTERS      IoCounters;
	SYSTEM_THREAD  Threads[1];
} SYSTEM_PROCESS_INFO, *PSYSTEM_PROCESS_INFO;

#endif


// original SYSTEM_PROCESS_INFORMATION
/*
typedef struct _SYSTEM_PROCESS_INFORMATION {
	ULONG NextEntryOffset;
	BYTE Reserved1[52];
	PVOID Reserved2[3];
	HANDLE UniqueProcessId;
	PVOID Reserved3; // parent process ID
	ULONG HandleCount;
	BYTE Reserved4[4]; // reserved2[0]
	PVOID Reserved5[11]; // reserved2[1] + vmcounters[0..10]
	SIZE_T PeakPagefileUsage; // vmcounters[11]
	SIZE_T PrivatePageCount;
	LARGE_INTEGER Reserved6[6]; // 6 * 8 IO_COUNTERS
} SYSTEM_PROCESS_INFORMATION, *PSYSTEM_PROCESS_INFORMATION;
*/