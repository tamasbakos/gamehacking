#include <Windows.h>
#include <Tlhelp32.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <time.h>

void toggleScreenMode(HWND hWindow = 0)
{
	if (hWindow != 0)
	{
		if (!SetForegroundWindow(hWindow))
		{
			std::cout << "Could not set application to foreground in screenmode toggle." << std::endl;
		}
	}

	INPUT input;
	
	input = { 0 };
	input.type = INPUT_KEYBOARD;
	input.ki.wVk = VK_MENU; // Alt key press

	SendInput(1, &input, sizeof(INPUT));

	input = { 0 };
	input.type = INPUT_KEYBOARD;
	input.ki.wVk = VK_RETURN; // Enter key press

	SendInput(1, &input, sizeof(INPUT));

	input = { 0 };
	input.type = INPUT_KEYBOARD;
	input.ki.wVk = VK_RETURN; // Enter key
	input.ki.dwFlags = KEYEVENTF_KEYUP; // release

	SendInput(1, &input, sizeof(INPUT));

	input = { 0 };
	input.type = INPUT_KEYBOARD;
	input.ki.wVk = VK_MENU; // Alt key
	input.ki.dwFlags = KEYEVENTF_KEYUP; // release

	SendInput(1, &input, sizeof(INPUT));
}

void mouseLeftClick()
{
	INPUT input = { 0 };
	input.type = INPUT_MOUSE;

	// left click
	input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP;

	SendInput(1, &input, sizeof(INPUT));
}

void mouseMove(const int x, const int y, std::string window)
{
	// calculate scale factor
	const double XSCALEFACTOR = 65535 / (GetSystemMetrics(SM_CXSCREEN) - 1);
	const double YSCALEFACTOR = 65535 / (GetSystemMetrics(SM_CYSCREEN) - 1);

	// get the window position
	RECT rect = { 0 };

	HWND hWindow = FindWindow(NULL, window.c_str());

	if (hWindow != NULL)
	{
		if (!GetWindowRect(hWindow, &rect));
	}
	
	// get current position
	POINT cursorPos;
	GetCursorPos(&cursorPos);
	double cx = cursorPos.x * XSCALEFACTOR;
	double cy = cursorPos.y * YSCALEFACTOR;

	// calculate target position relative to application or relative to upper left corner
	double nx = (x + rect.left) * XSCALEFACTOR;
	double ny = (y + rect.top) * YSCALEFACTOR;

	INPUT Input = { 0 };
	Input.type = INPUT_MOUSE;

	Input.mi.dx = (LONG)nx;
	Input.mi.dy = (LONG)ny;

	// set move cursor directly
	Input.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE;

	SendInput(1, &Input, sizeof(INPUT));
}


int main()
{
	Sleep(5000);
	
	std::string window = "Zuma Deluxe 1.0";
	HWND hWindow = NULL;
	// find window

	hWindow = FindWindow(NULL, window.c_str());

	if (NULL == hWindow) 
	{
		std::cout << "Could not find application window." << std::endl;
	}
	else
	{
		WINDOWINFO info;
		GetWindowInfo(hWindow, &info);

		if (info.dwExStyle & WS_EX_TOPMOST) // if maximized, exit full screen
		{
			toggleScreenMode();
		}

		WINDOWPLACEMENT p = { 0 };
		GetWindowPlacement(hWindow, &p);
		
		if (p.showCmd == SW_SHOWMINIMIZED || !IsWindowVisible(hWindow))
		{
			std::cout << "Window is minimized" << std::endl;
		}

		if (!SetForegroundWindow(hWindow)) 
		{
			std::cout << "Could not set application to foreground." << std::endl;
		}
		else
		{			

			Sleep(2000);
			// Click on click here to play
			mouseMove(250, 480, window);
			Sleep(1000);
			mouseLeftClick();
			Sleep(1000);

			// Click on Adventure
			mouseMove(550, 120, window);
			Sleep(1000);
			mouseLeftClick();
			Sleep(1000);

			// Click on New Game
			mouseMove(380, 360, window);
			Sleep(1000);
			mouseLeftClick();
			Sleep(1000);

			// Click on stage one
			mouseMove(260, 370, window);
			Sleep(1000);
			mouseLeftClick();
			Sleep(1000);

			// Click on play
			mouseMove(580, 490, window);
			Sleep(1000);
			mouseLeftClick();
			Sleep(1000);

			mouseMove(550, 180, window);
			Sleep(1000);

			while (true)
			{
				mouseMove(550, 180, window);
				mouseLeftClick();
				Sleep(200);
			}

			std::string str, str1, str2;

			while (str != "exit")
			{
				std::cin >> str;

				if (str == "mov")
				{
					std::cin >> str1 >> str2;
					int x = std::stoi(str1);
					int y = std::stoi(str2);

					mouseMove(x, y, window);
				}

				if (str == "left")
				{
					mouseLeftClick();
				}
				
				
				Sleep(500);
			}
		}
	}
	return 0;
}

