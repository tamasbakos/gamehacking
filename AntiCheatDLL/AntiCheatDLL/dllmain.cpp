#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <Psapi.h>
#include <TlHelp32.h>
#include <vector>
#include <string>
#include <fstream>
#include <set>

#define BREAK __asm int 3

#define TEXT_SECTION_OFFSET 0x00001000

static HMODULE dllModule;
static HANDLE currentProcess;

static HANDLE dumpDLLthread;
static HANDLE findPatchesThread;

static void dumpDLLs();
static void dumpPatches();

// TODO list potential cheating dlls

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		dllModule = hModule;
		currentProcess = GetCurrentProcess();

		dumpDLLthread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)dumpDLLs, NULL, 0, NULL);
		findPatchesThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)dumpPatches, NULL, 0, NULL);
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

static void dumpDLLs()
{
	std::set<HMODULE> dumpedModules;
	std::set<HMODULE> whitelistModules;
	std::set<HMODULE> blacklistModules;

	std::vector<HMODULE> modules(1024);
	DWORD modulesSize;

	wchar_t modName[MAX_PATH];
	wchar_t dumpPath[MAX_PATH];
	MODULEINFO modInfo;

	//generate whitelist of modules when anticheat dll loaded
	EnumProcessModulesEx(currentProcess, &modules[0], modules.size() * sizeof(HMODULE), &modulesSize, LIST_MODULES_ALL);
	for (int i = 0; i < modulesSize / sizeof(HMODULE); ++i)
	{
		whitelistModules.insert(modules[i]);
	}

	BOOL res = CreateDirectoryW(L"__DllDumps", NULL);

	wcscpy(dumpPath, L"__DllDumps\\__SuspectDlls.txt");
	HANDLE suspectDllsFile = CreateFileW(dumpPath, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);


	boolean loop = true;
	while (loop)
	{
		EnumProcessModulesEx(currentProcess, &modules[0], modules.size() * sizeof(HMODULE), &modulesSize, LIST_MODULES_ALL);
		for (int i = 0; i < modulesSize / sizeof(HMODULE); ++i)
		{
			if (dllModule == modules[i] || dumpedModules.find(modules[i]) != dumpedModules.end())
			{
				continue;
			}

			GetModuleBaseNameW(currentProcess, modules[i], modName, MAX_PATH);
			GetModuleInformation(currentProcess, modules[i], &modInfo, sizeof(MODULEINFO));

			DWORD len;
			if (whitelistModules.find(modules[i]) == whitelistModules.end() && 
				blacklistModules.find(modules[i]) == blacklistModules.end())
			{
				WriteFile(suspectDllsFile, modName, wcslen(modName) * sizeof(wchar_t), &len, NULL);
				WriteFile(suspectDllsFile, L"\r\n", wcslen(L"\r\n") * sizeof(wchar_t), &len, NULL);
				
				FlushFileBuffers(suspectDllsFile);
				blacklistModules.insert(modules[i]);
			}
			
			wcscpy(dumpPath, L"__DllDumps\\");
			wcscat(dumpPath, modName);
			wcscat(dumpPath, L".dump");

			HANDLE dumpfile = CreateFileW(dumpPath, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

			WriteFile(dumpfile, modInfo.lpBaseOfDll, modInfo.SizeOfImage, &len, NULL);

			dumpedModules.insert(modules[i]);

			CloseHandle(dumpfile);
		}
	}
}

static void dumpPatches()
{
	/*
	SYSTEM_INFO sys_info;
	GetSystemInfo(&sys_info);

	MEMORY_BASIC_INFORMATION mem_info;

	BYTE* addr = (BYTE*)0x0;
	std::wstring dumpfilename = L"executable.dump";

	HANDLE dumpfile = CreateFileW(dumpfilename.c_str(), GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	while (addr < sys_info.lpMaximumApplicationAddress)
	{
	VirtualQueryEx(currentProcess, addr, &mem_info, sizeof(MEMORY_BASIC_INFORMATION));

	if (mem_info.State == MEM_COMMIT && (mem_info.Protect & 0xFFFFFF0F))
	{
	DWORD len;
	WriteFile(dumpfile, mem_info.BaseAddress, mem_info.RegionSize, &len, NULL);

	}
	addr += mem_info.RegionSize;
	}

	CloseHandle(dumpfile);
	*/
	std::vector<HMODULE> modules(1024);
	DWORD modulesSize;

	MODULEINFO modInfo;


	EnumProcessModulesEx(currentProcess, &modules[0], modules.size() * sizeof(HMODULE), &modulesSize, LIST_MODULES_ALL);

	// implement later to check changes in DLLs
	/*
	for (int i = 1; i < modulesSize / sizeof(HMODULE); ++i)
	{
	GetModuleBaseNameW(currentProcess, modules[i], modName, MAX_PATH);
	GetModuleInformation(currentProcess, modules[i], &modInfo, sizeof(MODULEINFO));

	wcscat(modName, L".dump");

	HANDLE dumpfile = CreateFileW(modName, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	DWORD len;
	WriteFile(dumpfile, modInfo.lpBaseOfDll, modInfo.SizeOfImage, &len, NULL);


	CloseHandle(dumpfile);
	}
	*/

	GetModuleInformation(currentProcess, modules[0], &modInfo, sizeof(MODULEINFO));

	IMAGE_DOS_HEADER* dos;
	IMAGE_NT_HEADERS* nt;
	IMAGE_SECTION_HEADER* sectheader;
	IMAGE_SECTION_HEADER* textsectheader;
	BYTE* textsect;

	std::set<BYTE*> loggedChanges;

	dos = (IMAGE_DOS_HEADER*)modInfo.lpBaseOfDll;

	if (IMAGE_DOS_SIGNATURE != dos->e_magic) //MZ
	{
		BREAK;
		return;
	}

	nt = (IMAGE_NT_HEADERS*)((DWORD)dos + dos->e_lfanew);

	if (IMAGE_NT_SIGNATURE != nt->Signature) //PE00
	{
		BREAK;
		return;
	}

	WORD nrSect = nt->FileHeader.NumberOfSections;

	sectheader = (IMAGE_SECTION_HEADER*)((BYTE*)nt + 4 + sizeof(IMAGE_FILE_HEADER) + nt->FileHeader.SizeOfOptionalHeader);


	wchar_t modName[MAX_PATH];
	GetModuleBaseNameW(currentProcess, modules[0], modName, MAX_PATH);
	wcscat(modName, L".text.dump");
	HANDLE codedumpfile = CreateFileW(modName, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	
	textsectheader = NULL;
	for (int s = 0; s < nrSect; s++)
	{
		if (sectheader[s].VirtualAddress == TEXT_SECTION_OFFSET)
		{
			textsectheader = &sectheader[s];
			break;
		}
	}
	
	if (textsectheader == NULL)
	{
		//BREAK;
		return;
	}

	textsect = (BYTE*)((DWORD)modInfo.lpBaseOfDll + textsectheader->VirtualAddress);

	DWORD len;
	WriteFile(codedumpfile, (LPCVOID)((DWORD)modInfo.lpBaseOfDll + textsectheader->VirtualAddress), textsectheader->Misc.VirtualSize, &len, NULL);
	
	FlushFileBuffers(codedumpfile);
	
	HANDLE changelogfile = CreateFileW(L"changelog.txt", GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	char line[512];
	strcpy(line, "Offset    Orig Patched\r\n\r\n");
	WriteFile(changelogfile, line, strlen(line), &len, NULL);

	boolean loop = true;
	while (loop)
	{
		SetFilePointer(codedumpfile, 0, NULL, FILE_BEGIN);

		for (int i = 0; i < textsectheader->Misc.VirtualSize; ++i)
		{
			BYTE orig;
			ReadFile(codedumpfile, &orig, sizeof(BYTE), &len, NULL);

			BYTE* addr = (BYTE*)((DWORD)textsect + i);
			char templ[] = "%08X:  %2X   %2X\r\n";
			if (orig != *addr && loggedChanges.find(addr) == loggedChanges.end())
			{
				DWORD offset = (DWORD)dos > (DWORD)addr ? (DWORD)dos - (DWORD)addr : (DWORD)addr - (DWORD)dos;
				sprintf(line, templ, offset , orig, *addr);
				DWORD len;
				WriteFile(changelogfile, line, strlen(line), &len, NULL);

				loggedChanges.insert(addr);
			}
		}
		strcpy(line, "\r\n");
		WriteFile(changelogfile, line, strlen(line), &len, NULL);
		FlushFileBuffers(changelogfile);

	}
	

}