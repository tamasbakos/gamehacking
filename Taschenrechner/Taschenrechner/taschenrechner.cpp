#include "taschenrechner.h"

Taschenrechner::Taschenrechner(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.lineEdit->setText("0");
	reset();
	this->setFixedSize(340, 400);
	this->setFocus();
}

Taschenrechner::~Taschenrechner()
{
}

void Taschenrechner::keyPressEvent(QKeyEvent *e)
{
	switch (e->key())
	{
	case Qt::Key_0:
	case Qt::Key_1:
	case Qt::Key_2:
	case Qt::Key_3:
	case Qt::Key_4:
	case Qt::Key_5:
	case Qt::Key_6:
	case Qt::Key_7:
	case Qt::Key_8:
	case Qt::Key_9:
		addNr(e->key() - Qt::Key_0);
		break;
	case Qt::Key_Backspace:
		pushButton_back();
		break;
	case Qt::Key_Delete:
		pushButton_ce();
		break;
	case Qt::Key_Return:
	case Qt::Key_Enter:
		pushButton_result();
		break;
	case Qt::Key_Plus:
		pushButton_add();
		break;
	case Qt::Key_Minus:
		pushButton_sub();
		break;
	case Qt::Key_Asterisk:
		pushButton_mul();
		break;
	case Qt::Key_Slash:
		pushButton_div();
		break;
	default:
		break;
	}
}

void Taschenrechner::reset()
{
	currentText = "0";
	status = 0;
	first_number = 0;
	second_number = 0;
	calcOperator = 0;

	first_text = "0";
	second_text = "";
}

void Taschenrechner::addNr(int i)
{
	if (status == 0) //Erste Zahl
	{
		if (first_text == "0")
		{
			first_text = "";
			currentText = "";
		}
		first_text += QString::number(i);
		first_number = first_text.toInt();

	}
	else if (status == 1) //Zweite Zahl
	{
		second_text += QString::number(i);
		second_number = second_text.toInt();
	}

	currentText += QString::number(i);
	ui.lineEdit->setText(currentText);
}

void Taschenrechner::addOperator(char op)
{
	if (status == 1 && !second_text.isEmpty())
	{
		return;
	}

	status = 1;

	if (calcOperator == 0)
	{
		currentText += " " + QString(op) + " ";
	}
	else
	{
		currentText.truncate(currentText.length() - 2);
		currentText += QString(op) + " ";
	}

	calcOperator = op;
	ui.lineEdit->setText(currentText);
}

void Taschenrechner::pushButton_nr0()
{
	addNr(0);
}
void Taschenrechner::pushButton_nr1()
{
	addNr(1);
}
void Taschenrechner::pushButton_nr2()
{
	addNr(2);
}
void Taschenrechner::pushButton_nr3()
{
	addNr(3);
}
void Taschenrechner::pushButton_nr4()
{
	addNr(4);
}
void Taschenrechner::pushButton_nr5()
{
	addNr(5);
}
void Taschenrechner::pushButton_nr6()
{
	addNr(6);
}
void Taschenrechner::pushButton_nr7()
{
	addNr(7);
}
void Taschenrechner::pushButton_nr8()
{
	addNr(8);
}
void Taschenrechner::pushButton_nr9()
{
	addNr(9);
}

void Taschenrechner::pushButton_result()
{
	if (second_text == "")
	{
		return;
	}

	int result;
	if (status == 0)
	{
		return;
	}

	switch (calcOperator)
	{
	case '+':
		result = first_number + second_number;
		break;
	case '-':
		result = first_number - second_number;
		break;
	case '*':
		result = first_number * second_number;
		break;
	case '/':
		if (second_number == 0)
		{
			ui.lineEdit->setText(QString(QChar(0x221E)));
			reset();
			return;
		}

		result = first_number / second_number;
		break;
	case '^':
		result = (int)pow(first_number, second_number);
		break;

	default:
		break;
	}

	ui.lineEdit->setText(QString::number(result));
	reset();

	first_number = result;
	first_text = currentText = QString::number(result);
}

void Taschenrechner::pushButton_add()
{
	addOperator('+');
}
void Taschenrechner::pushButton_sub()
{
	addOperator('-');
}
void Taschenrechner::pushButton_div()
{
	addOperator('/');
}
void Taschenrechner::pushButton_mul()
{
	addOperator('*');
}
void Taschenrechner::pushButton_pow()
{
	addOperator('^');
}

void Taschenrechner::pushButton_back()
{
	if (currentText[currentText.length() - 1] == ' ')
	{
		currentText.truncate(currentText.length() - 3);
		calcOperator = 0;
		status = 0;
	}
	else if (status == 1)
	{
		currentText.truncate(currentText.length() - 1);
		second_text.truncate(second_text.length() - 1);

		if (!second_text.isEmpty())
		{
			second_number = second_text.toInt();
		}
		else
		{
			second_number = 0;
		}
	}
	else if (status == 0)
	{
		currentText.truncate(currentText.length() - 1);
		first_text.truncate(first_text.length() - 1);
		if (!first_text.isEmpty())
		{
			first_number = first_text.toInt();
		}
		else
		{
			first_number = 0;
		}
	}

	if (currentText.isEmpty())
	{
		ui.lineEdit->setText("0");
		reset();
		return;
	}

	ui.lineEdit->setText(currentText);
}

void Taschenrechner::pushButton_ce()
{
	ui.lineEdit->setText("0");
	reset();
}

void Taschenrechner::pushButton_sign()
{
	if (status == 0)
	{
		first_number = -first_number;
		first_text = QString::number(first_number);
		currentText = first_text;
	}
	else if (status == 1)
	{
		second_number = -second_number;
		second_text = QString::number(second_number);
		currentText = first_text + " " + calcOperator + " " + second_text;
	}

	ui.lineEdit->setText(currentText);
}