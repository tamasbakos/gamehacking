#ifndef TASCHENRECHNER_H
#define TASCHENRECHNER_H
#include <string>
#include <cmath>
#include <QtWidgets/QMainWindow>
#include <QKeyEvent>
#include "ui_taschenrechner.h"

class Taschenrechner : public QMainWindow
{
	Q_OBJECT

public:
	Taschenrechner(QWidget *parent = 0);
	~Taschenrechner();

private:
	Ui::TaschenrechnerClass ui;
	
	void keyPressEvent(QKeyEvent *event);

	QString currentText;

	int status;
	int first_number;
	int second_number;
	char calcOperator;

	QString first_text;
	QString second_text;

	void addNr(int i);
	void addOperator(char op);

	void reset();

	public slots:;
	void pushButton_nr0(); //Nr 0 - 9
	void pushButton_nr1();
	void pushButton_nr2();
	void pushButton_nr3();
	void pushButton_nr4();
	void pushButton_nr5();
	void pushButton_nr6();
	void pushButton_nr7();
	void pushButton_nr8();
	void pushButton_nr9();

	void pushButton_back(); //Del Last letter
	void pushButton_ce(); //Clear Everything
	void pushButton_add(); //Addition
	void pushButton_sub(); //Substraction
	void pushButton_mul(); //Multiplication
	void pushButton_div(); //Division
	void pushButton_pow(); //Power
	void pushButton_result(); //Displays result
	void pushButton_sign(); //Change sign

};

#endif // TASCHENRECHNER_H
