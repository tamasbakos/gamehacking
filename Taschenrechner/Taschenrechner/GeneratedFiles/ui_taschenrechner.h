/********************************************************************************
** Form generated from reading UI file 'taschenrechner.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASCHENRECHNER_H
#define UI_TASCHENRECHNER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TaschenrechnerClass
{
public:
    QAction *actionEnable_Keyboard;
    QWidget *centralWidget;
    QLineEdit *lineEdit;
    QLabel *label;
    QPushButton *pushButton_ce;
    QPushButton *pushButton_nr7;
    QPushButton *pushButton_nr8;
    QPushButton *pushButton_nr9;
    QPushButton *pushButton_nr4;
    QPushButton *pushButton_nr5;
    QPushButton *pushButton_nr6;
    QPushButton *pushButton_nr1;
    QPushButton *pushButton_nr2;
    QPushButton *pushButton_nr3;
    QPushButton *pushButton_nr0;
    QPushButton *pushButton_sign;
    QPushButton *pushButton_div;
    QPushButton *pushButton_mul;
    QPushButton *pushButton_sub;
    QPushButton *pushButton_add;
    QPushButton *pushButton_result;
    QPushButton *pushButton_back;
    QPushButton *pushButton_pow;
    QMenuBar *menuBar;

    void setupUi(QMainWindow *TaschenrechnerClass)
    {
        if (TaschenrechnerClass->objectName().isEmpty())
            TaschenrechnerClass->setObjectName(QStringLiteral("TaschenrechnerClass"));
        TaschenrechnerClass->resize(340, 400);
        actionEnable_Keyboard = new QAction(TaschenrechnerClass);
        actionEnable_Keyboard->setObjectName(QStringLiteral("actionEnable_Keyboard"));
        actionEnable_Keyboard->setCheckable(true);
        actionEnable_Keyboard->setChecked(true);
        centralWidget = new QWidget(TaschenrechnerClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(20, 40, 231, 61));
        QFont font;
        font.setPointSize(12);
        lineEdit->setFont(font);
        lineEdit->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lineEdit->setReadOnly(true);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 0, 261, 41));
        QFont font1;
        font1.setPointSize(18);
        label->setFont(font1);
        pushButton_ce = new QPushButton(centralWidget);
        pushButton_ce->setObjectName(QStringLiteral("pushButton_ce"));
        pushButton_ce->setGeometry(QRect(80, 110, 51, 41));
        pushButton_ce->setFont(font);
        pushButton_nr7 = new QPushButton(centralWidget);
        pushButton_nr7->setObjectName(QStringLiteral("pushButton_nr7"));
        pushButton_nr7->setGeometry(QRect(20, 160, 51, 41));
        pushButton_nr7->setFont(font);
        pushButton_nr8 = new QPushButton(centralWidget);
        pushButton_nr8->setObjectName(QStringLiteral("pushButton_nr8"));
        pushButton_nr8->setGeometry(QRect(80, 160, 51, 41));
        pushButton_nr8->setFont(font);
        pushButton_nr9 = new QPushButton(centralWidget);
        pushButton_nr9->setObjectName(QStringLiteral("pushButton_nr9"));
        pushButton_nr9->setGeometry(QRect(140, 160, 51, 41));
        pushButton_nr9->setFont(font);
        pushButton_nr4 = new QPushButton(centralWidget);
        pushButton_nr4->setObjectName(QStringLiteral("pushButton_nr4"));
        pushButton_nr4->setGeometry(QRect(20, 210, 51, 41));
        pushButton_nr4->setFont(font);
        pushButton_nr5 = new QPushButton(centralWidget);
        pushButton_nr5->setObjectName(QStringLiteral("pushButton_nr5"));
        pushButton_nr5->setGeometry(QRect(80, 210, 51, 41));
        pushButton_nr5->setFont(font);
        pushButton_nr6 = new QPushButton(centralWidget);
        pushButton_nr6->setObjectName(QStringLiteral("pushButton_nr6"));
        pushButton_nr6->setGeometry(QRect(140, 210, 51, 41));
        pushButton_nr6->setFont(font);
        pushButton_nr1 = new QPushButton(centralWidget);
        pushButton_nr1->setObjectName(QStringLiteral("pushButton_nr1"));
        pushButton_nr1->setGeometry(QRect(20, 260, 51, 41));
        pushButton_nr1->setFont(font);
        pushButton_nr2 = new QPushButton(centralWidget);
        pushButton_nr2->setObjectName(QStringLiteral("pushButton_nr2"));
        pushButton_nr2->setGeometry(QRect(80, 260, 51, 41));
        pushButton_nr2->setFont(font);
        pushButton_nr3 = new QPushButton(centralWidget);
        pushButton_nr3->setObjectName(QStringLiteral("pushButton_nr3"));
        pushButton_nr3->setGeometry(QRect(140, 260, 51, 41));
        pushButton_nr3->setFont(font);
        pushButton_nr0 = new QPushButton(centralWidget);
        pushButton_nr0->setObjectName(QStringLiteral("pushButton_nr0"));
        pushButton_nr0->setGeometry(QRect(20, 310, 111, 41));
        pushButton_nr0->setFont(font);
        pushButton_sign = new QPushButton(centralWidget);
        pushButton_sign->setObjectName(QStringLiteral("pushButton_sign"));
        pushButton_sign->setGeometry(QRect(200, 110, 51, 41));
        pushButton_sign->setFont(font);
        pushButton_div = new QPushButton(centralWidget);
        pushButton_div->setObjectName(QStringLiteral("pushButton_div"));
        pushButton_div->setGeometry(QRect(200, 160, 51, 41));
        pushButton_div->setFont(font);
        pushButton_mul = new QPushButton(centralWidget);
        pushButton_mul->setObjectName(QStringLiteral("pushButton_mul"));
        pushButton_mul->setGeometry(QRect(200, 210, 51, 41));
        pushButton_mul->setFont(font);
        pushButton_sub = new QPushButton(centralWidget);
        pushButton_sub->setObjectName(QStringLiteral("pushButton_sub"));
        pushButton_sub->setGeometry(QRect(200, 260, 51, 41));
        pushButton_sub->setFont(font);
        pushButton_add = new QPushButton(centralWidget);
        pushButton_add->setObjectName(QStringLiteral("pushButton_add"));
        pushButton_add->setGeometry(QRect(200, 310, 51, 41));
        pushButton_add->setFont(font);
        pushButton_result = new QPushButton(centralWidget);
        pushButton_result->setObjectName(QStringLiteral("pushButton_result"));
        pushButton_result->setGeometry(QRect(270, 260, 51, 91));
        QFont font2;
        font2.setPointSize(14);
        font2.setBold(true);
        font2.setWeight(75);
        pushButton_result->setFont(font2);
        pushButton_back = new QPushButton(centralWidget);
        pushButton_back->setObjectName(QStringLiteral("pushButton_back"));
        pushButton_back->setGeometry(QRect(20, 110, 51, 41));
        pushButton_back->setFont(font);
        pushButton_pow = new QPushButton(centralWidget);
        pushButton_pow->setObjectName(QStringLiteral("pushButton_pow"));
        pushButton_pow->setGeometry(QRect(140, 110, 51, 41));
        TaschenrechnerClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(TaschenrechnerClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 340, 21));
        TaschenrechnerClass->setMenuBar(menuBar);

        retranslateUi(TaschenrechnerClass);
        QObject::connect(pushButton_back, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_back()));
        QObject::connect(pushButton_ce, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_ce()));
        QObject::connect(pushButton_div, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_div()));
        QObject::connect(pushButton_mul, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_mul()));
        QObject::connect(pushButton_sub, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_sub()));
        QObject::connect(pushButton_add, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_add()));
        QObject::connect(pushButton_result, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_result()));
        QObject::connect(pushButton_nr0, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_nr0()));
        QObject::connect(pushButton_nr1, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_nr1()));
        QObject::connect(pushButton_nr2, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_nr2()));
        QObject::connect(pushButton_nr3, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_nr3()));
        QObject::connect(pushButton_nr4, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_nr4()));
        QObject::connect(pushButton_nr5, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_nr5()));
        QObject::connect(pushButton_nr6, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_nr6()));
        QObject::connect(pushButton_nr7, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_nr7()));
        QObject::connect(pushButton_nr8, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_nr8()));
        QObject::connect(pushButton_nr9, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_nr9()));
        QObject::connect(pushButton_sign, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_sign()));
        QObject::connect(pushButton_pow, SIGNAL(clicked()), TaschenrechnerClass, SLOT(pushButton_pow()));

        QMetaObject::connectSlotsByName(TaschenrechnerClass);
    } // setupUi

    void retranslateUi(QMainWindow *TaschenrechnerClass)
    {
        TaschenrechnerClass->setWindowTitle(QApplication::translate("TaschenrechnerClass", "Taschenrechner", 0));
        actionEnable_Keyboard->setText(QApplication::translate("TaschenrechnerClass", "Enable Keyboard", 0));
        label->setText(QApplication::translate("TaschenrechnerClass", "Taschenrechner V 1.0", 0));
        pushButton_ce->setText(QApplication::translate("TaschenrechnerClass", "CE", 0));
        pushButton_nr7->setText(QApplication::translate("TaschenrechnerClass", "7", 0));
        pushButton_nr8->setText(QApplication::translate("TaschenrechnerClass", "8", 0));
        pushButton_nr9->setText(QApplication::translate("TaschenrechnerClass", "9", 0));
        pushButton_nr4->setText(QApplication::translate("TaschenrechnerClass", "4", 0));
        pushButton_nr5->setText(QApplication::translate("TaschenrechnerClass", "5", 0));
        pushButton_nr6->setText(QApplication::translate("TaschenrechnerClass", "6", 0));
        pushButton_nr1->setText(QApplication::translate("TaschenrechnerClass", "1", 0));
        pushButton_nr2->setText(QApplication::translate("TaschenrechnerClass", "2", 0));
        pushButton_nr3->setText(QApplication::translate("TaschenrechnerClass", "3", 0));
        pushButton_nr0->setText(QApplication::translate("TaschenrechnerClass", "0", 0));
        pushButton_sign->setText(QApplication::translate("TaschenrechnerClass", "+/-", 0));
        pushButton_div->setText(QApplication::translate("TaschenrechnerClass", "/", 0));
        pushButton_mul->setText(QApplication::translate("TaschenrechnerClass", "*", 0));
        pushButton_sub->setText(QApplication::translate("TaschenrechnerClass", "-", 0));
        pushButton_add->setText(QApplication::translate("TaschenrechnerClass", "+", 0));
        pushButton_result->setText(QApplication::translate("TaschenrechnerClass", "=", 0));
        pushButton_back->setText(QApplication::translate("TaschenrechnerClass", "<-", 0));
        pushButton_pow->setText(QApplication::translate("TaschenrechnerClass", "^", 0));
    } // retranslateUi

};

namespace Ui {
    class TaschenrechnerClass: public Ui_TaschenrechnerClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASCHENRECHNER_H
