#include "entryeditor.h"

EntryEditor::EntryEditor(QWidget *parent) : QDialog(parent)
{
	ui.setupUi(this);
}

EntryEditor::~EntryEditor()
{
}

QString EntryEditor::getNewValue()
{
	return ui.lineEdit_newValue->text();
}

void EntryEditor::setOldValue(QString str)
{
	ui.lineEdit_oldValue->setText(str);
}

void EntryEditor::setNewValue(QString str)
{
	ui.lineEdit_newValue->setText(str);
}