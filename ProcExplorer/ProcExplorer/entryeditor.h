#ifndef ENTRYEDITOR_H
#define ENTRYEDITOR_H

#include <QDialog>
#include "ui_entryeditor.h"

class EntryEditor : public QDialog
{
	Q_OBJECT

public:
	EntryEditor(QWidget *parent = 0);
	~EntryEditor();

	QString getNewValue();
	void setOldValue(QString str);
	void setNewValue(QString str);

private:
	Ui::EntryEditor ui;
};

#endif // ENTRYEDITOR_H
