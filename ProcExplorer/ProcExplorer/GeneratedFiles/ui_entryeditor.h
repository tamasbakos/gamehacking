/********************************************************************************
** Form generated from reading UI file 'entryeditor.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ENTRYEDITOR_H
#define UI_ENTRYEDITOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_EntryEditor
{
public:
    QDialogButtonBox *buttonBox;
    QLineEdit *lineEdit_newValue;
    QLabel *label_new;
    QLabel *label_old;
    QLineEdit *lineEdit_oldValue;

    void setupUi(QDialog *EntryEditor)
    {
        if (EntryEditor->objectName().isEmpty())
            EntryEditor->setObjectName(QStringLiteral("EntryEditor"));
        EntryEditor->resize(427, 153);
        EntryEditor->setModal(true);
        buttonBox = new QDialogButtonBox(EntryEditor);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(90, 100, 181, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        lineEdit_newValue = new QLineEdit(EntryEditor);
        lineEdit_newValue->setObjectName(QStringLiteral("lineEdit_newValue"));
        lineEdit_newValue->setGeometry(QRect(70, 70, 331, 20));
        label_new = new QLabel(EntryEditor);
        label_new->setObjectName(QStringLiteral("label_new"));
        label_new->setGeometry(QRect(20, 70, 51, 21));
        label_old = new QLabel(EntryEditor);
        label_old->setObjectName(QStringLiteral("label_old"));
        label_old->setGeometry(QRect(20, 30, 51, 21));
        lineEdit_oldValue = new QLineEdit(EntryEditor);
        lineEdit_oldValue->setObjectName(QStringLiteral("lineEdit_oldValue"));
        lineEdit_oldValue->setGeometry(QRect(70, 30, 331, 20));
        lineEdit_oldValue->setReadOnly(true);

        retranslateUi(EntryEditor);
        QObject::connect(buttonBox, SIGNAL(accepted()), EntryEditor, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), EntryEditor, SLOT(reject()));

        QMetaObject::connectSlotsByName(EntryEditor);
    } // setupUi

    void retranslateUi(QDialog *EntryEditor)
    {
        EntryEditor->setWindowTitle(QApplication::translate("EntryEditor", "EntryEditor", 0));
        label_new->setText(QApplication::translate("EntryEditor", "New", 0));
        label_old->setText(QApplication::translate("EntryEditor", "Old", 0));
    } // retranslateUi

};

namespace Ui {
    class EntryEditor: public Ui_EntryEditor {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ENTRYEDITOR_H
