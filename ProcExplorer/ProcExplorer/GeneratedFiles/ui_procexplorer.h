/********************************************************************************
** Form generated from reading UI file 'procexplorer.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROCEXPLORER_H
#define UI_PROCEXPLORER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ProcExplorerClass
{
public:
    QWidget *centralWidget;
    QComboBox *comboBox_procList;
    QLabel *label_procList;
    QTableView *tableView_foundValues;
    QPushButton *pushButton_reloadProcList;
    QPushButton *pushButton_openProc;
    QPushButton *pushButton_firstScan;
    QPushButton *pushButton_nextScan;
    QLineEdit *lineEdit_value;
    QLabel *label_value;
    QComboBox *comboBox_valueType;
    QLabel *label_valueType;
    QTableView *tableView_changeableValues;
    QPushButton *pushButton_abortScan;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QComboBox *comboBox_scanType;
    QLabel *label_scanType;
    QLineEdit *lineEdit_changedBy;
    QLineEdit *lineEdit_searchProc;
    QPushButton *pushButton_searchProc;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *ProcExplorerClass)
    {
        if (ProcExplorerClass->objectName().isEmpty())
            ProcExplorerClass->setObjectName(QStringLiteral("ProcExplorerClass"));
        ProcExplorerClass->resize(721, 650);
        ProcExplorerClass->setMinimumSize(QSize(600, 650));
        centralWidget = new QWidget(ProcExplorerClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        comboBox_procList = new QComboBox(centralWidget);
        comboBox_procList->setObjectName(QStringLiteral("comboBox_procList"));
        comboBox_procList->setGeometry(QRect(20, 30, 371, 22));
        label_procList = new QLabel(centralWidget);
        label_procList->setObjectName(QStringLiteral("label_procList"));
        label_procList->setGeometry(QRect(30, 10, 111, 16));
        tableView_foundValues = new QTableView(centralWidget);
        tableView_foundValues->setObjectName(QStringLiteral("tableView_foundValues"));
        tableView_foundValues->setGeometry(QRect(20, 100, 371, 281));
        tableView_foundValues->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView_foundValues->setSelectionMode(QAbstractItemView::SingleSelection);
        tableView_foundValues->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableView_foundValues->setShowGrid(true);
        tableView_foundValues->verticalHeader()->setVisible(false);
        tableView_foundValues->verticalHeader()->setDefaultSectionSize(24);
        tableView_foundValues->verticalHeader()->setMinimumSectionSize(20);
        pushButton_reloadProcList = new QPushButton(centralWidget);
        pushButton_reloadProcList->setObjectName(QStringLiteral("pushButton_reloadProcList"));
        pushButton_reloadProcList->setGeometry(QRect(410, 30, 111, 23));
        pushButton_openProc = new QPushButton(centralWidget);
        pushButton_openProc->setObjectName(QStringLiteral("pushButton_openProc"));
        pushButton_openProc->setGeometry(QRect(550, 30, 91, 23));
        pushButton_firstScan = new QPushButton(centralWidget);
        pushButton_firstScan->setObjectName(QStringLiteral("pushButton_firstScan"));
        pushButton_firstScan->setEnabled(false);
        pushButton_firstScan->setGeometry(QRect(410, 100, 75, 23));
        pushButton_nextScan = new QPushButton(centralWidget);
        pushButton_nextScan->setObjectName(QStringLiteral("pushButton_nextScan"));
        pushButton_nextScan->setEnabled(false);
        pushButton_nextScan->setGeometry(QRect(500, 100, 75, 23));
        lineEdit_value = new QLineEdit(centralWidget);
        lineEdit_value->setObjectName(QStringLiteral("lineEdit_value"));
        lineEdit_value->setGeometry(QRect(480, 140, 191, 20));
        label_value = new QLabel(centralWidget);
        label_value->setObjectName(QStringLiteral("label_value"));
        label_value->setGeometry(QRect(420, 140, 47, 16));
        comboBox_valueType = new QComboBox(centralWidget);
        comboBox_valueType->setObjectName(QStringLiteral("comboBox_valueType"));
        comboBox_valueType->setGeometry(QRect(480, 170, 191, 22));
        label_valueType = new QLabel(centralWidget);
        label_valueType->setObjectName(QStringLiteral("label_valueType"));
        label_valueType->setGeometry(QRect(410, 170, 61, 21));
        tableView_changeableValues = new QTableView(centralWidget);
        tableView_changeableValues->setObjectName(QStringLiteral("tableView_changeableValues"));
        tableView_changeableValues->setGeometry(QRect(20, 410, 681, 171));
        tableView_changeableValues->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableView_changeableValues->setSelectionMode(QAbstractItemView::SingleSelection);
        tableView_changeableValues->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableView_changeableValues->verticalHeader()->setVisible(false);
        pushButton_abortScan = new QPushButton(centralWidget);
        pushButton_abortScan->setObjectName(QStringLiteral("pushButton_abortScan"));
        pushButton_abortScan->setEnabled(false);
        pushButton_abortScan->setGeometry(QRect(600, 100, 75, 23));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(470, 380, 101, 23));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(584, 380, 111, 23));
        comboBox_scanType = new QComboBox(centralWidget);
        comboBox_scanType->setObjectName(QStringLiteral("comboBox_scanType"));
        comboBox_scanType->setGeometry(QRect(480, 200, 111, 22));
        label_scanType = new QLabel(centralWidget);
        label_scanType->setObjectName(QStringLiteral("label_scanType"));
        label_scanType->setGeometry(QRect(410, 200, 61, 21));
        lineEdit_changedBy = new QLineEdit(centralWidget);
        lineEdit_changedBy->setObjectName(QStringLiteral("lineEdit_changedBy"));
        lineEdit_changedBy->setGeometry(QRect(600, 200, 71, 21));
        lineEdit_searchProc = new QLineEdit(centralWidget);
        lineEdit_searchProc->setObjectName(QStringLiteral("lineEdit_searchProc"));
        lineEdit_searchProc->setGeometry(QRect(20, 70, 251, 20));
        pushButton_searchProc = new QPushButton(centralWidget);
        pushButton_searchProc->setObjectName(QStringLiteral("pushButton_searchProc"));
        pushButton_searchProc->setGeometry(QRect(280, 70, 111, 21));
        ProcExplorerClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(ProcExplorerClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 721, 21));
        ProcExplorerClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(ProcExplorerClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        ProcExplorerClass->setStatusBar(statusBar);

        retranslateUi(ProcExplorerClass);
        QObject::connect(pushButton_reloadProcList, SIGNAL(clicked()), ProcExplorerClass, SLOT(loadProcessList()));
        QObject::connect(pushButton_openProc, SIGNAL(clicked()), ProcExplorerClass, SLOT(opencloseProcess()));
        QObject::connect(pushButton_firstScan, SIGNAL(clicked()), ProcExplorerClass, SLOT(firstScan()));
        QObject::connect(pushButton_nextScan, SIGNAL(clicked()), ProcExplorerClass, SLOT(nextScan()));
        QObject::connect(tableView_changeableValues, SIGNAL(doubleClicked(QModelIndex)), ProcExplorerClass, SLOT(editValue(QModelIndex)));
        QObject::connect(pushButton_abortScan, SIGNAL(clicked()), ProcExplorerClass, SLOT(abortScan()));
        QObject::connect(tableView_foundValues, SIGNAL(doubleClicked(QModelIndex)), ProcExplorerClass, SLOT(addAddress(QModelIndex)));
        QObject::connect(pushButton, SIGNAL(clicked()), ProcExplorerClass, SLOT(removeAddress()));
        QObject::connect(lineEdit_searchProc, SIGNAL(returnPressed()), pushButton_searchProc, SLOT(click()));
        QObject::connect(pushButton_searchProc, SIGNAL(clicked()), ProcExplorerClass, SLOT(searchProcess()));

        QMetaObject::connectSlotsByName(ProcExplorerClass);
    } // setupUi

    void retranslateUi(QMainWindow *ProcExplorerClass)
    {
        ProcExplorerClass->setWindowTitle(QApplication::translate("ProcExplorerClass", "ProcExplorer", 0));
        label_procList->setText(QApplication::translate("ProcExplorerClass", "Select process:", 0));
        pushButton_reloadProcList->setText(QApplication::translate("ProcExplorerClass", "Reload Process List", 0));
        pushButton_openProc->setText(QApplication::translate("ProcExplorerClass", "Open Process", 0));
        pushButton_firstScan->setText(QApplication::translate("ProcExplorerClass", "First Scan", 0));
        pushButton_nextScan->setText(QApplication::translate("ProcExplorerClass", "Next Scan", 0));
        label_value->setText(QApplication::translate("ProcExplorerClass", "Value", 0));
        label_valueType->setText(QApplication::translate("ProcExplorerClass", "Value Type", 0));
        pushButton_abortScan->setText(QApplication::translate("ProcExplorerClass", "Abort Scan", 0));
        pushButton->setText(QApplication::translate("ProcExplorerClass", "Remove Address", 0));
        pushButton_2->setText(QApplication::translate("ProcExplorerClass", "Clear Address List", 0));
        label_scanType->setText(QApplication::translate("ProcExplorerClass", "Scan Type", 0));
        lineEdit_searchProc->setPlaceholderText(QApplication::translate("ProcExplorerClass", " Process name", 0));
        pushButton_searchProc->setText(QApplication::translate("ProcExplorerClass", "Search Process", 0));
    } // retranslateUi

};

namespace Ui {
    class ProcExplorerClass: public Ui_ProcExplorerClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROCEXPLORER_H
