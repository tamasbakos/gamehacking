#include "procexplorer.h"

ProcExplorer::ProcExplorer(QWidget *parent) : QMainWindow(parent)
{
	ui.setupUi(this);

	// Setup table of found values

	model_foundValues = new QStandardItemModel(0, 2, this); // 0 Rows and 2 Columns
	model_foundValues->setHorizontalHeaderItem(0, new QStandardItem(QString("Address")));
	model_foundValues->setHorizontalHeaderItem(1, new QStandardItem(QString("Previous Value")));
	model_foundValues->setHorizontalHeaderItem(2, new QStandardItem(QString("Current Value")));

	ui.tableView_foundValues->setModel(model_foundValues);

	double width = ui.tableView_foundValues->width() - 18;
	ui.tableView_foundValues->setColumnWidth(0, 0.26*width);
	ui.tableView_foundValues->setColumnWidth(1, 0.37*width);
	ui.tableView_foundValues->setColumnWidth(2, 0.37*width);

	// Setup table of changeable values

	model_changeableValues = new QStandardItemModel(0, 3, this); // 0 Rows and 2 Columns

	model_changeableValues->setHorizontalHeaderItem(0, new QStandardItem(QString("Address")));
	model_changeableValues->setHorizontalHeaderItem(1, new QStandardItem(QString("Type")));
	model_changeableValues->setHorizontalHeaderItem(2, new QStandardItem(QString("Current Value")));

	ui.tableView_changeableValues->setModel(model_changeableValues);

	ui.tableView_changeableValues->setColumnWidth(0, 0.25*width);
	ui.tableView_changeableValues->setColumnWidth(1, 0.5*width);
	ui.tableView_changeableValues->setColumnWidth(2, 1 * width - 18);

	// Setup type selection combobox

	ui.comboBox_valueType->addItem(QString("Byte[1]"));
	ui.comboBox_valueType->addItem(QString("Word[2]"));
	ui.comboBox_valueType->addItem(QString("Dword[4]"));
	ui.comboBox_valueType->addItem(QString("Qword[8]"));

	ui.comboBox_valueType->addItem(QString("Float[4]"));
	ui.comboBox_valueType->addItem(QString("Double[8]"));

	ui.comboBox_valueType->addItem(QString("String(ASCII)[]"));
	ui.comboBox_valueType->addItem(QString("String(Unicode)[]"));

	ui.comboBox_valueType->setCurrentIndex(1);

	// Setup scan type selection

	ui.comboBox_scanType->addItem(QString("Exact Value"));
	ui.comboBox_scanType->addItem(QString("Unknown Initial Value"));

	ui.comboBox_scanType->setCurrentIndex(0);

	ui.comboBox_scanType->hide(); // TODO implement unknown initial value
	ui.label_scanType->hide();
	ui.lineEdit_changedBy->hide();

	GetSystemInfo(&sys_info);

	loadProcessList();

	processHandle = NULL;

	updateFoundValuesThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)updateFoundValues, this, CREATE_SUSPENDED, NULL);
	updateChangeableValuesThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)updateChangeableValues, this, CREATE_SUSPENDED, NULL);

	foundValuesMutex = CreateMutex(NULL, FALSE, NULL);
	changeableValuesMutex = CreateMutex(NULL, FALSE, NULL);
}

ProcExplorer::~ProcExplorer()
{
	if (processHandle)
	{
		opencloseProcess();
	}

	TerminateThread(updateFoundValuesThread, 0);
	TerminateThread(updateChangeableValuesThread, 0);

	CloseHandle(updateFoundValuesThread);
	CloseHandle(updateChangeableValuesThread);

	CloseHandle(foundValuesMutex);
	CloseHandle(changeableValuesMutex);

	delete model_foundValues;
	delete model_changeableValues;
}

QString valueToString(BYTE* addr, QString type, int size)
{
	if (type.startsWith("Byte"))
	{
		return QString::number(*(quint8*)addr);
	}
	else if (type.startsWith("Word"))
	{
		return QString::number(*(quint16*)addr);
	}
	else if (type.startsWith("Dword"))
	{
		return QString::number(*(quint32*)addr);
	}
	else if (type.startsWith("Qword"))
	{
		return QString::number(*(quint64*)addr);
	}
	else if (type.startsWith("Float"))
	{
		return QString::number(*(float*)addr);
	}
	else if (type.startsWith("Double"))
	{
		return QString::number(*(double*)addr);
	}
	else if (type.startsWith("String(ASCII)"))
	{
		return QString::fromLocal8Bit((char*)addr, size);
	}
	else if (type.startsWith("String(Unicode)"))
	{
		return QString::fromWCharArray((wchar_t*)addr, size / 2).replace(QChar('\0'), QChar(0xF8));
	}

	return "";
}

std::vector<BYTE> stringToValue(QString string, QString type)
{
	std::vector<BYTE> ret;
	BYTE* addr;

	if (type.startsWith("Byte"))
	{
		ret.resize(1);
		addr = &ret[0];

		*(quint8*)addr = (quint8)string.toInt();
	}
	else if (type.startsWith("Word"))
	{
		ret.resize(2);
		addr = &ret[0];

		*(quint16*)addr = string.toUShort();
	}
	else if (type.startsWith("Dword"))
	{
		ret.resize(4);
		addr = &ret[0];

		*(quint32*)addr = string.toUInt();
	}
	else if (type.startsWith("Qword"))
	{
		ret.resize(8);
		addr = &ret[0];

		*(quint64*)addr = string.toLongLong();
	}
	else if (type.startsWith("Float"))
	{
		ret.resize(4);
		addr = &ret[0];

		*(float*)addr = string.toFloat();
	}
	else if (type.startsWith("Double"))
	{
		ret.resize(8);
		addr = &ret[0];

		*(double*)addr = string.toDouble();
	}
	else if (type.startsWith("String(ASCII)"))
	{
		ret.resize(string.length());
		addr = &ret[0];

		std::string str = string.toStdString();
		strncpy((char*)addr, str.c_str(), str.size());
	}
	else if (type.startsWith("String(Unicode)"))
	{
		ret.resize(string.length() * 2);
		addr = &ret[0];

		std::wstring str = string.toStdWString();
		wcsncpy((wchar_t*)addr, str.c_str(), str.size());
	}

	return ret;
}

void ProcExplorer::loadProcessList()
{
	ui.comboBox_procList->clear();
	bool showUnknownProc = false;

	DWORD bytesReturned, nrProcesses;
	std::vector<DWORD> processIds(1024);

	if (!EnumProcesses(&processIds[0], processIds.size(), &bytesReturned))
	{
		return;
	}

	// Calculate how many process identifiers were returned.
	nrProcesses = bytesReturned / sizeof(DWORD);

	// Put the name and process identifier for each process into the combobox.
	std::vector<QString> pnames;

	for (int i = 0; i < nrProcesses; i++)
	{
		if (processIds[i] != 0)
		{
			// Get a handle to the process.
			HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processIds[i]);

			// Get the process name.
			TCHAR processName[MAX_PATH] = TEXT("<unknown>");

			if (NULL != hProcess)
			{
				HMODULE hMod;
				DWORD cbNeeded;

				if (EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cbNeeded))
				{
					GetModuleBaseName(hProcess, hMod, processName, MAX_PATH);
				}
			}

			QString pidstr = QString::number(processIds[i]);
			QString procname = QString::fromWCharArray(processName);

			while (pidstr.length() < 5) pidstr = "0" + pidstr;

			if (procname != "<unknown>" || showUnknownProc)
			{
				pnames.push_back(procname.toLower() + "|" + pidstr + " : " + procname);
			}

			CloseHandle(hProcess);
		}
	}

	std::sort(pnames.begin(), pnames.end());

	for (auto pname : pnames)
	{
		ui.comboBox_procList->addItem(pname.split("|")[1]);
	}
}

void ProcExplorer::searchProcess()
{
	QString toSearch = ui.lineEdit_searchProc->text().toLower();

	if (toSearch.isEmpty())
	{
		return;
	}

	int currentInd = ui.comboBox_procList->currentIndex();

	for (int i = currentInd + 1; i < ui.comboBox_procList->count(); ++i)
	{
		QString currentName = ui.comboBox_procList->itemText(i).toLower();

		if (currentName.contains(toSearch))
		{
			ui.comboBox_procList->setCurrentIndex(i);
			return;
		}
	}

	for (int i = 0; i < currentInd + 1; ++i)
	{
		QString currentName = ui.comboBox_procList->itemText(i).toLower();

		if (currentName.contains(toSearch))
		{
			ui.comboBox_procList->setCurrentIndex(i);
			return;
		}
	}

	loadProcessList();

	for (int i = 0; i < ui.comboBox_procList->count(); ++i)
	{
		QString currentName = ui.comboBox_procList->itemText(i).toLower();

		if (currentName.contains(toSearch))
		{
			ui.comboBox_procList->setCurrentIndex(i);
			return;
		}
	}
}


void ProcExplorer::opencloseProcess()
{
	QString pidstr = ui.comboBox_procList->currentText().split(":")[0].trimmed();
	DWORD pid = pidstr.toULong();

	if (processHandle == NULL) // open process
	{
		if (NULL == (processHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid)))
		{
			return; // unsuccessful open
		}

		ui.comboBox_procList->setEnabled(false);
		ui.pushButton_reloadProcList->setEnabled(false);
		ui.pushButton_firstScan->setEnabled(true);
		ui.pushButton_nextScan->setEnabled(false);

		ui.pushButton_openProc->setText("Close Process");

		ResumeThread(updateChangeableValuesThread);
	}
	else // close process
	{
		CloseHandle(processHandle);
		processHandle = NULL;

		abortScan();

		ui.comboBox_procList->setEnabled(true);
		ui.pushButton_reloadProcList->setEnabled(true);
		ui.pushButton_firstScan->setEnabled(false);
		ui.pushButton_nextScan->setEnabled(false);

		ui.pushButton_openProc->setText("Open Process");

		model_foundValues->removeRows(0, model_foundValues->rowCount());
		model_changeableValues->removeRows(0, model_changeableValues->rowCount());

		SuspendThread(updateChangeableValuesThread);
	}

}


void ProcExplorer::firstScan()
{
	QString datastr = ui.lineEdit_value->text();

	if (datastr == "")
	{
		return;
	}

	ui.pushButton_firstScan->setEnabled(false);
	ui.pushButton_nextScan->setEnabled(true);
	ui.pushButton_abortScan->setEnabled(true);

	ui.comboBox_valueType->setEnabled(false);

	QString type = ui.comboBox_valueType->currentText();
	QString valuestr = ui.lineEdit_value->text();

	if (type.startsWith("String"))
	{
		type = type.split("[")[0] + "[" + QString::number(valuestr.length()) + "]";
		ui.comboBox_valueType->setItemText(ui.comboBox_valueType->currentIndex(), type);
	}

	std::vector<BYTE> value = stringToValue(valuestr, type);
	BYTE* valueaddr = &value[0];

	MEMORY_BASIC_INFORMATION mem_info;
	std::vector<BYTE> chunk;

	BYTE* addr = (BYTE*)sys_info.lpMinimumApplicationAddress;

	while (addr < sys_info.lpMaximumApplicationAddress)
	{
		VirtualQueryEx(processHandle, addr, &mem_info, sizeof(MEMORY_BASIC_INFORMATION));

		if (mem_info.Protect == PAGE_READWRITE && mem_info.State == MEM_COMMIT)
		{
			chunk.resize(mem_info.RegionSize);

			ReadProcessMemory(processHandle, mem_info.BaseAddress, &chunk[0], mem_info.RegionSize, NULL);

			BYTE* chunkaddr = &chunk[0];

			for (SIZE_T i = 0; i < mem_info.RegionSize; ++i)
			{

				if (memcmp(valueaddr, chunkaddr + i, value.size()) == 0)
				{
					model_foundValues->appendRow(new QStandardItem(1, 3));
					model_foundValues->setItem(model_foundValues->rowCount() - 1, 0, new QStandardItem("0x" + QString::number((SIZE_T)(addr + i), 16).toUpper()));
					model_foundValues->setItem(model_foundValues->rowCount() - 1, 1, new QStandardItem(valuestr));

				}
			}
		}
		addr += mem_info.RegionSize;
	}

	if (model_foundValues->rowCount() == 0)
	{
		abortScan();
	}

	ResumeThread(updateFoundValuesThread);
}

void ProcExplorer::nextScan()
{
	QString valuestr = ui.lineEdit_value->text();
	QString type = ui.comboBox_valueType->currentText();

	if (valuestr == "" || model_foundValues->rowCount() == 0)
	{
		return;
	}
	
	std::vector<BYTE> value = stringToValue(valuestr, type);

	std::vector<BYTE> buffer(value.size());

	WaitForSingleObject(foundValuesMutex, INFINITE);

	for (int r = model_foundValues->rowCount() - 1; r >= 0; --r)
	{
		QString addrStr = model_foundValues->item(r, 0)->text();

		BYTE* addr = (BYTE*)addrStr.toLongLong(NULL, 16);

		ReadProcessMemory(processHandle, addr, &buffer[0], value.size(), NULL);

		//delete row here
		model_foundValues->removeRows(r, 1);

		if (memcmp(&value[0], &buffer[0], value.size()) == 0)
		{
			model_foundValues->appendRow(new QStandardItem(1, 3));
			model_foundValues->setItem(model_foundValues->rowCount() - 1, 0, new QStandardItem("0x" + QString::number((SIZE_T)addr, 16).toUpper()));
			model_foundValues->setItem(model_foundValues->rowCount() - 1, 1, new QStandardItem(ui.lineEdit_value->text()));
		}
	}

	ReleaseMutex(foundValuesMutex);
}

void ProcExplorer::abortScan()
{
	if (!ui.pushButton_abortScan->isEnabled())
	{
		return;
	}

	WaitForSingleObject(foundValuesMutex, INFINITE);

	model_foundValues->removeRows(0, model_foundValues->rowCount());
	SuspendThread(updateFoundValuesThread);

	ReleaseMutex(foundValuesMutex);
	
	QString type = ui.comboBox_valueType->currentText();

	if (type.startsWith("String"))
	{
		type = type.split("[")[0] + "[]";
		ui.comboBox_valueType->setItemText(ui.comboBox_valueType->currentIndex(), type);
	}

	ui.pushButton_firstScan->setEnabled(true);
	ui.pushButton_nextScan->setEnabled(false);
	ui.pushButton_abortScan->setEnabled(false);

	ui.comboBox_valueType->setEnabled(true);

	ui.comboBox_scanType->clear();
	ui.comboBox_scanType->addItem(QString("Exact Value"));
	ui.comboBox_scanType->addItem(QString("Unknown Initial Value"));
}

void ProcExplorer::addAddress(QModelIndex index)
{
	WaitForSingleObject(changeableValuesMutex, INFINITE);

	model_changeableValues->appendRow(new QStandardItem(1, 3));

	QString address = model_foundValues->item(index.row(), 0)->text();
	QString previous = model_foundValues->item(index.row(), 1)->text();
	QString type = ui.comboBox_valueType->currentText();

	model_changeableValues->setItem(model_changeableValues->rowCount() - 1, 0, new QStandardItem(address));
	model_changeableValues->setItem(model_changeableValues->rowCount() - 1, 1, new QStandardItem(type));

	ReleaseMutex(changeableValuesMutex);
}

void ProcExplorer::removeAddress()
{
	WaitForSingleObject(changeableValuesMutex, INFINITE);

	QModelIndex index = ui.tableView_changeableValues->currentIndex();
	int row = index.row();
	int col = index.column();

	for (int r = row; r < model_changeableValues->rowCount() - 1; ++r)
	{
		model_changeableValues->setItem(r, 0, new QStandardItem(model_changeableValues->item(r + 1, 0)->text()));
		model_changeableValues->setItem(r, 1, new QStandardItem(model_changeableValues->item(r + 1, 1)->text()));
		model_changeableValues->setItem(r, 2, new QStandardItem(model_changeableValues->item(r + 1, 2)->text()));
	}

	model_changeableValues->removeRows(model_changeableValues->rowCount() - 1, 1);

	ReleaseMutex(changeableValuesMutex);
}

void ProcExplorer::editValue(QModelIndex index)
{
	int row = index.row();
	int col = index.column();

	EntryEditor editor;
	editor.setOldValue(model_changeableValues->item(row, col)->text());
	editor.setNewValue(model_changeableValues->item(row, col)->text());

	switch (col)
	{
	case 1: // edit type
		editor.setWindowTitle("Edit type");
		break;
	case 0: // edit address
		editor.setWindowTitle("Edit address");
		break;
	case 2://edit value
		editor.setWindowTitle("Edit value");
		break;
	default:
		break;
	}

	if (!editor.exec()) //cancel clicked
	{
		return;
	}

	QString newValue = editor.getNewValue();

	if (col == 0 || col == 1) // edited address or type
	{
		WaitForSingleObject(changeableValuesMutex, INFINITE);
		
		model_changeableValues->setItem(row, col, new QStandardItem(newValue));
		
		ReleaseMutex(changeableValuesMutex);
		return;
	}

	if (col == 2) // edit value
	{
		std::vector<BYTE> buffer = stringToValue(newValue, model_changeableValues->item(row, 1)->text());

		QString addrStr = model_changeableValues->item(row, 0)->text();
		BYTE* addr = (BYTE*)addrStr.toLongLong(NULL, 16);

		WriteProcessMemory(processHandle, addr, &buffer[0], buffer.size(), NULL);
	}
}

void ProcExplorer::updateFoundValues(void* param)
{
	ProcExplorer* This = (ProcExplorer*)param;

	Ui::ProcExplorerClass* thisui = &This->ui;
	QStandardItemModel* thismodel_foundValues = This->model_foundValues;

	while (true)
	{
		QString type = thisui->comboBox_valueType->currentText();
		int size = type.split("[")[1].split("]")[0].toInt();

		if (type.contains("Unicode"))
		{
			size *= 2;
		}

		std::vector<BYTE> buffer(size);
		BYTE* bufferaddr = &buffer[0];

		WaitForSingleObject(This->foundValuesMutex, INFINITE);

		QModelIndex index = thisui->tableView_foundValues->currentIndex();

		for (int r = 0; r < thismodel_foundValues->rowCount(); ++r)
		{
			QString addrStr = thismodel_foundValues->item(r, 0)->text();
			BYTE* addr = (BYTE*)addrStr.toLongLong(NULL, 16);

			DWORD err;
			if (!ReadProcessMemory(This->processHandle, addr, bufferaddr, size, NULL))
			{
				err = GetLastError();
			}

			thismodel_foundValues->setItem(r, 2, new QStandardItem(valueToString(bufferaddr, thisui->comboBox_valueType->currentText(), size)));
		}

		//force update
		thismodel_foundValues->appendRow(new QStandardItem(1, 3));
		thismodel_foundValues->removeRows(thismodel_foundValues->rowCount() - 1, 1);

		thisui->tableView_foundValues->setCurrentIndex(index);

		ReleaseMutex(This->foundValuesMutex);

		Sleep(400);
	}
}

void ProcExplorer::updateChangeableValues(void* param)
{
	ProcExplorer* This = (ProcExplorer*)param;

	Ui::ProcExplorerClass* thisui = &This->ui;
	QStandardItemModel* thismodel_changeableValues = This->model_changeableValues;
	
	while (true)
	{
		WaitForSingleObject(This->changeableValuesMutex, INFINITE);

		QModelIndex index = thisui->tableView_changeableValues->currentIndex();

		for (int r = 0; r < thismodel_changeableValues->rowCount(); ++r)
		{
			QString type = thismodel_changeableValues->item(r, 1)->text();
			int size = type.split("[")[1].split("]")[0].toInt();

			if (type.contains("Unicode"))
			{
				size *= 2;
			}

			std::vector<BYTE> buffer(size);

			QString addrStr = thismodel_changeableValues->item(r, 0)->text();
			BYTE* addr = (BYTE*)addrStr.toLongLong(NULL, 16);

			DWORD err;
			if (!ReadProcessMemory(This->processHandle, addr, &buffer[0], size, NULL))
			{
				err = GetLastError();
			}

			thismodel_changeableValues->setItem(r, 2, new QStandardItem(valueToString(&buffer[0], type, size)));
		}

		//force update
		thismodel_changeableValues->appendRow(new QStandardItem(1, 3));
		thismodel_changeableValues->removeRows(thismodel_changeableValues->rowCount() - 1, 1);

		thisui->tableView_changeableValues->setCurrentIndex(index);

		ReleaseMutex(This->changeableValuesMutex);

		Sleep(500);
	}
}

