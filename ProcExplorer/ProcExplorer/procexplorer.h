#ifndef PROCEXPLORER_H
#define PROCEXPLORER_H

#include <QtWidgets/QMainWindow>
#include <QStandardItemModel>
#include <Windows.h>
#include <Psapi.h>
#include <string>
#include <cctype>
#include <vector>
#include <algorithm>
#include "ui_procexplorer.h"

#include "entryeditor.h"

class ProcExplorer : public QMainWindow
{
	Q_OBJECT

public:
	ProcExplorer(QWidget *parent = 0);
	~ProcExplorer();

private:
	Ui::ProcExplorerClass ui;
	QStandardItemModel *model_foundValues;
	QStandardItemModel *model_changeableValues;

	SYSTEM_INFO sys_info;
	HANDLE processHandle;

	HANDLE updateFoundValuesThread;
	HANDLE updateChangeableValuesThread;

	HANDLE foundValuesMutex;
	HANDLE changeableValuesMutex;

	static void updateFoundValues(void* param);
	static void updateChangeableValues(void* param);

	signals:
	void updateList();

	public slots:

	void loadProcessList();
	void searchProcess();
	void opencloseProcess();

	void firstScan();
	void nextScan();
	void abortScan();

	void addAddress(QModelIndex index);
	void removeAddress();
	void editValue(QModelIndex index);


};

#endif // PROCEXPLORER_H
